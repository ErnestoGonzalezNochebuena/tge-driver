package taxigoexpress.com.driver

import android.os.Bundle
import com.taxigoexpress.core.presentation.activities.BaseActivity
import org.jetbrains.anko.intentFor

class SplashActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_splash_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.startActivity(intentFor<HomeActivity>())
        this.finish()
    }
}
