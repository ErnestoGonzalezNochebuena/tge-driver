package taxigoexpress.com.driver.login.data.repositoryImplementations

import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.login.data.datasourceImplementations.LoginDataSourceImpl
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.entities.responseEntities.LoginResponse
import taxigoexpress.com.driver.login.domain.repositoryAbstractions.LoginRepository
import io.reactivex.Observable
import taxigoexpress.com.driver.login.domain.entities.responseEntities.DriverRequestEntity
import javax.inject.Inject

class LoginRepositoryImpl
@Inject constructor(private val signinDatasourceImpl: LoginDataSourceImpl): LoginRepository {

    override fun login(params: LoginRequest): Observable<Unit> =
        this.signinDatasourceImpl.login(params)

    override fun validateDriver(params: String): Observable<LoginResponse> =
        this.signinDatasourceImpl.validateDriver(params)


    override fun getProfile(params: String): Observable<UserEntity> =
        this.signinDatasourceImpl.getProfile(params)

    override fun generateDriverRequest(driverId: String): Observable<DriverRequestEntity> =
    this.signinDatasourceImpl.generateDriverRequest(driverId)


}