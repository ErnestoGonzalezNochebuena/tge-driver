package taxigoexpress.com.driver.login.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import taxigoexpress.com.driver.login.presentation.fragments.InitFragment
import taxigoexpress.com.driver.login.presentation.fragments.LoginFragment
import taxigoexpress.com.driver.login.presentation.modules.LoginModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [LoginModule::class]
)
interface LoginComponent {
    fun inject(loginFragment: LoginFragment)
    fun inject(initFragment: InitFragment)
}