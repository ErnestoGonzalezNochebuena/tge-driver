package taxigoexpress.com.driver.login.presentation.viewmodels.implementations

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.useCases.LoginUseCase
import taxigoexpress.com.driver.login.presentation.liveData.LoginLiveData
import taxigoexpress.com.driver.login.presentation.observers.LoginObserver
import taxigoexpress.com.driver.login.presentation.viewmodels.abstractions.LoginViewModel
import javax.inject.Inject

class LoginViewModelImpl
@Inject constructor(
    private val loginUseCase: LoginUseCase
) : BaseViewModelLiveData<UserEntity>(), LoginViewModel<UserEntity> {

    private val mSigninLiveData by lazy {
        LoginLiveData(this.getCustomLiveData()) }

    override fun login(request: LoginRequest) {
        this.loginUseCase.execute(
            LoginObserver(this.mSigninLiveData),
            request
        )
    }

    override fun observeLogin(): LoginLiveData =
        this.mSigninLiveData

    override fun observeResult(): BaseLiveData<UserEntity> =
        this.mSigninLiveData.loginObserver

    override fun onCleared() {
        this.loginUseCase.dispose()
        super.onCleared()
    }
}