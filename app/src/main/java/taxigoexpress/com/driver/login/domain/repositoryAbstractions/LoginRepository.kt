package taxigoexpress.com.driver.login.domain.repositoryAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import taxigoexpress.com.driver.login.domain.entities.responseEntities.DriverRequestEntity


interface LoginRepository {
    fun login(params: LoginRequest): Observable<Unit>
    fun validateDriver(params: String): Observable<LoginResponse>
    fun getProfile(params: String): Observable<UserEntity>
    fun generateDriverRequest(driverId: String): Observable<DriverRequestEntity>
}
