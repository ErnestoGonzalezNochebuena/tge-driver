package taxigoexpress.com.driver.login.domain.entities.responseEntities

import com.google.gson.annotations.SerializedName

data class DriverRequestEntity(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("conductor_id")
    val driverID: String? = null,

    @field:SerializedName("estatus")
    val status: String? = null,

    @field:SerializedName("notas")
    val extras: String? = null
)