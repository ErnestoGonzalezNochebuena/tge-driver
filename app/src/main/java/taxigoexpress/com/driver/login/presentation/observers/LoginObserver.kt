package taxigoexpress.com.driver.login.presentation.observers

import android.view.View
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.login.domain.exceptions.LoginException
import taxigoexpress.com.driver.login.presentation.liveData.LoginLiveData
import java.io.IOException

class LoginObserver(private val mLoginLiveData: LoginLiveData) :
    DefaultObserverLiveData<UserEntity>(mLoginLiveData.loginObserver) {

    override fun onStart() {
        this.mLoginLiveData.loginObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mLoginLiveData.loginObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: UserEntity) {
        this.mLoginLiveData.loginObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mLoginLiveData.loginObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mLoginLiveData.loginObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is LoginException -> useCase(error)
            else -> this.mLoginLiveData.loginObserver.showExceptionObserver.value =
                error.localizedMessage
        }
    }

    private fun useCase(error: LoginException) {
        when (error.validationType) {
            LoginException.Type.PASSWORD_EMPTY_ERROR -> this.mLoginLiveData.passwordErrorObserver.value =
                R.string.sign_in_empty_password_error
            LoginException.Type.EMAIL_WRONG_FORMAT -> this.mLoginLiveData.emailErrorObserver.value =
                R.string.sign_in_wrong_email_error
            LoginException.Type.NOT_DRIVER -> this.mLoginLiveData.noPassengerValue.value =
                R.string.no_valid_driver_error
            LoginException.Type.NOT_DRIVER_ACTIVE -> this.mLoginLiveData.noPassengerValue.value =
                R.string.no_driver_active
        }
    }

}
