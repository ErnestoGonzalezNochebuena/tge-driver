package taxigoexpress.com.driver.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.presentation.activities.LoginActivity
import taxigoexpress.com.driver.login.presentation.components.LoginComponent
import taxigoexpress.com.driver.login.presentation.modules.LoginModule
import taxigoexpress.com.driver.login.presentation.viewmodels.abstractions.LoginViewModel
import taxigoexpress.com.driver.register.presentation.fragments.RegisterFragment
import kotlinx.android.synthetic.main.fragment_login_layout.*
import org.jetbrains.anko.support.v4.intentFor
import taxigoexpress.com.driver.HomeActivity
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.login.presentation.components.DaggerLoginComponent
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : BaseFragment() {

    @Inject
    lateinit var loginViewModel: LoginViewModel<UserEntity>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    private val loginComponent: LoginComponent by lazy {
        DaggerLoginComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .loginModule(LoginModule())
            .build()
    }


    override fun getLayout(): Int = R.layout.fragment_login_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        loginComponent.inject(this)
        this.configureToolbar()
        this.loadView()
        this.observeResults()
    }

    private fun configureToolbar() {
        (this.activity as LoginActivity).showToolbar()
    }

    private fun loadView() {
        this.btnForgotPassword.clickEvent().observe(this, Observer {

        })

        this.btnLogin.clickEvent().observe(this, Observer {
            this.loginViewModel.login(
                LoginRequest(
                    this.etMail.text.toString(),
                    this.etPassword.text.toString()
                )
            )
        })
    }

    private fun observeResults() {
        with(this.loginViewModel.observeLogin()) {
            this.loginObserver.customObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.USER,
                    this@LoginFragment.etMail.text.toString()
                )

                this@LoginFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.DRIVER_INFO,
                    Gson().toJson(it, UserEntity::class.java)
                )

                this@LoginFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.DRIVER_ID,
                    it._id.toString()
                )

                this@LoginFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.PASS,
                    this@LoginFragment.etPassword.text.toString()
                )
                this@LoginFragment.activity?.finish()
                this@LoginFragment.startActivity(intentFor<HomeActivity>())
            })

            this.loginObserver.loadingObserver.observe(this@LoginFragment, Observer {
                if (it == View.VISIBLE) {
                    this@LoginFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@LoginFragment.loaderDialog.dismiss()
                }
            })


            this.loginObserver.showErrorObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })

            this.loginObserver.showExceptionObserver.observe(
                this@LoginFragment,
                Observer {
                    this@LoginFragment.showAlert("Error al inicio de sesión")
                })

            this.emailErrorObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })

            this.noPassengerValue.observe(this@LoginFragment, Observer {
                this@LoginFragment.showAlertWithResource(it ?: 0)
            })
        }
    }

    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }


}
