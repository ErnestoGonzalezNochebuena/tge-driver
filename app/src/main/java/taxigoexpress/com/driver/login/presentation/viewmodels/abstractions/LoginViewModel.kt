package taxigoexpress.com.driver.login.presentation.viewmodels.abstractions

import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.presentation.liveData.LoginLiveData

interface LoginViewModel<T> : PresenterLiveData<T> {
    fun login(request: LoginRequest)
    fun observeLogin(): LoginLiveData
}