package taxigoexpress.com.driver.login.data.datasourceImplementations

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.managers.ResponseManager
import taxigoexpress.com.driver.login.data.services.LoginServices
import taxigoexpress.com.driver.login.domain.datasourceAbstractions.LoginDataSource
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import taxigoexpress.com.driver.login.domain.entities.responseEntities.DriverRequestEntity
import javax.inject.Inject

class LoginDataSourceImpl
@Inject constructor(private val service: LoginServices) : LoginDataSource {

    override fun login(params: LoginRequest): Observable<Unit> =
        this.service.login(params)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(Unit)
                } else {
                    Observable.error(ResponseManager.processWrongBody(it))
                }
            }

    override fun validateDriver(email: String): Observable<LoginResponse> =
        this.service.validateDriver(email)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(ResponseManager.processWrongBody(it))
                }
            }

    override fun getProfile(params: String): Observable<UserEntity> =
        this.service.getProfile(params)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(ResponseManager.processWrongBody(it))
                }
            }

    override fun generateDriverRequest(driverId: String): Observable<DriverRequestEntity> =
        this.service.generateDriverRequest(driverId)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(ResponseManager.processWrongBody(it))
                }
            }


}
