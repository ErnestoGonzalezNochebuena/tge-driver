package taxigoexpress.com.driver.login.domain.datasourceAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import taxigoexpress.com.driver.login.domain.entities.responseEntities.DriverRequestEntity

interface LoginDataSource {
    fun login(params: LoginRequest): Observable<Unit>
    fun validateDriver(email:String):Observable<LoginResponse>
    fun getProfile(params: String): Observable<UserEntity>
    fun generateDriverRequest(driverId: String): Observable<DriverRequestEntity>

}
