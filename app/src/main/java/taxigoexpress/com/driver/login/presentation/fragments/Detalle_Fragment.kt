package taxigoexpress.com.driver.login.presentation.fragments




import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import taxigoexpress.com.driver.login.presentation.activities.DetallesActivity
import kotlinx.android.synthetic.main.fragment_login_layout.*
import taxigoexpress.com.driver.R

/**
 * A simple [Fragment] subclass.
 */
class Detalle_Fragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_detalles_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.loadView()
    }

    private fun configureToolbar() {
        (this.activity as DetallesActivity).showToolbar()
    }

    private fun loadView() {
        /*this.btnForgotPassword.clickEvent().observe(this, Observer {
            this.replaceFragment(
                PasswordRecoveryFragment(),
                R.id.login_container,
                true
            )
        })*/

        this.btnLogin.clickEvent().observe(this, Observer {
            this.activity?.finish()
        })
    }

    companion object {
        fun newInstance(): Detalle_Fragment {
            return Detalle_Fragment()
        }
    }


}
