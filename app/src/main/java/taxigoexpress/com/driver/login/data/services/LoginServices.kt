package taxigoexpress.com.driver.login.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.BaseResponse
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import taxigoexpress.com.driver.login.domain.entities.responseEntities.DriverRequestEntity

interface LoginServices {

    @POST(BuildConfig.LOGIN)
    fun login(
        @Body loginRequest: LoginRequest
    ):Observable<Response<Unit>>

    @GET(BuildConfig.VALIDATE_DRIVER)
    fun validateDriver(
        @Path("email") email: String
    ): Observable<Response<LoginResponse>>

    @GET(BuildConfig.GET_PROFILE)
    fun getProfile(
        @Path("id") profileId: String
    ): Observable<Response<UserEntity>>

    @POST(BuildConfig.GENERATE_DRIVER_REQUEST)
    fun generateDriverRequest(
        @Path("id_conductor") profileId: String
    ): Observable<Response<DriverRequestEntity>>
}