package taxigoexpress.com.driver.login.domain.useCases

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.components.SessionData
import com.taxigoexpress.core.presentation.utils.isValidEmail
import taxigoexpress.com.driver.login.domain.entities.requestEntities.LoginRequest
import taxigoexpress.com.driver.login.domain.exceptions.LoginException
import taxigoexpress.com.driver.login.domain.repositoryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginUseCase
@Inject constructor(
    private val loginRepository: LoginRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<UserEntity, LoginRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: LoginRequest): Observable<UserEntity> = when {
        params._password.isEmpty() -> Observable.error(LoginException(LoginException.Type.PASSWORD_EMPTY_ERROR))
        !params._email.isValidEmail() -> Observable.error(LoginException(LoginException.Type.EMAIL_WRONG_FORMAT))
        else ->
            this.loginRepository.validateDriver(params = params._email)
                .flatMap {
                    if (it._userType != 4) {
                        Observable.error(LoginException(LoginException.Type.NOT_DRIVER))
                    } else {
                        SessionData.driverId = it._driverId
                        this.loginRepository.login(params).flatMap {
                            this.loginRepository.getProfile(SessionData.driverId.toString())
                                .flatMap { driver ->
                                    if (driver._status?.toLowerCase() != "activo") {
                                        Observable.error(LoginException(LoginException.Type.NOT_DRIVER_ACTIVE))
                                    } else {
                                        Observable.just(driver)
                                    }
                                }
                        }
                    }
                }
    }
}