package taxigoexpress.com.driver.register.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import taxigoexpress.com.driver.register.domain.entities.KeysEntity
import taxigoexpress.com.driver.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetAwsKeysUseCase
@Inject constructor(
    private val repository: RegisterRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<KeysEntity, Unit>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Unit): Observable<KeysEntity> =
        this.repository.getAwsKeys()
}