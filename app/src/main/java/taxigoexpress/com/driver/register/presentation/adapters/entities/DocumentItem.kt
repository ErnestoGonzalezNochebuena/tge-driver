package taxigoexpress.com.driver.register.presentation.adapters.entities

import taxigoexpress.com.driver.register.presentation.aws.Document

data class DocumentItem(
    val type: Document,
    val isAdded: Boolean
) {
    val status get() = if (isAdded) "Agregado" else "Sin registro"
}