package taxigoexpress.com.driver.register.presentation.viewmodels.abstractions

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import taxigoexpress.com.driver.register.presentation.liveData.RegisterCarLiveData

interface RegisterCarViewModel<T> : PresenterLiveData<T> {
    fun registerCar(request: CarEntity)
    fun observeRegisterCar(): RegisterCarLiveData
}