package taxigoexpress.com.driver.register.presentation.aws

import android.content.Context
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobile.client.UserStateDetails
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import taxigoexpress.com.driver.register.presentation.aws.AwsKeys.BUCKET_NAME
import java.io.File


class AwsCore(private val applicationContext: Context) {

    private lateinit var amazonS3: AmazonS3Client

    private fun initializeAWS() {
        val credentialsProvider =
            CognitoCachingCredentialsProvider(
                applicationContext,
                AwsKeys.IDENTITY_POOL,  // Identity Pool ID
                Regions.US_EAST_2 // Region
            )
        amazonS3 = AmazonS3Client(credentialsProvider, Region.getRegion(Regions.US_EAST_2))
    }

    init {
        initializeAWS()
    }

    fun uploadDocument(
        document: Document,
        file: File,
        documentPath: String,
        callback: UploadCallback
    ) {
        val transferUtility = TransferUtility.builder()
            .s3Client(amazonS3)
            .context(applicationContext)
            .build()

        val uploadObserver = transferUtility.upload(
            BUCKET_NAME,
            documentPath,
            file,
            CannedAccessControlList.PublicRead //to make the file public
        )

        uploadObserver.setTransferListener(object : TransferListener {
            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val percentDonef =
                    bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                callback.onGetCurrentPercentage(percentDonef.toInt())
            }

            override fun onStateChanged(id: Int, state: TransferState?) {
                when (state) {
                    TransferState.COMPLETED -> callback.onSuccessUpload(document)
                    TransferState.CANCELED -> callback.onUploadFailed(
                        document,
                        "Operation $id Canceled"
                    )
                    TransferState.FAILED -> callback.onUploadFailed(
                        document,
                        "Operation $id Failed"
                    )
                    TransferState.UNKNOWN -> {}
                    else -> {}
                }
            }

            override fun onError(id: Int, ex: Exception?) {
                ex?.localizedMessage?.let {
                    callback.onUploadFailed(
                        document,
                        it
                    )
                } ?: kotlin.run {
                    callback.onUploadFailed(
                        document,
                        "Uploading failure"
                    )
                }
            }
        })

        callback.onStartUpload()

    }

    private fun getDocumentName(document: Document): String =
        when (document) {
            Document.License -> "licencia.pdf"
            Document.CardCirculation -> "tarjeta_de_circulacion.pdf"
            Document.InsurancePolicy -> "poliza_de_seguro.pdf"
            Document.AddressDocument -> "comprobante_de_domicilio.pdf"
            else -> "document.pdf"
        }


    private fun getS3Path(
        document: Document,
        documentPath: String
    ): String = String.format(
        "%s/%s",
        documentPath,
        getDocumentName(document)
    )


    interface InitializeCallback {
        fun onSuccessInitialization(userStateDetails: UserStateDetails)
        fun onError(error: String?)
    }

    interface UploadCallback {
        fun onSuccessUpload(document: Document)
        fun onGetCurrentPercentage(percentage: Int)
        fun onUploadFailed(document: Document, reason: String)
        fun onStartUpload()
    }
}

