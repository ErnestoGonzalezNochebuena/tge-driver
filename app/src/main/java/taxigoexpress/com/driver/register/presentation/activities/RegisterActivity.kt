package taxigoexpress.com.driver.register.presentation.activities

import android.os.Bundle
import androidx.lifecycle.Observer
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.utils.clickEvent
import taxigoexpress.com.driver.register.presentation.fragments.RegisterFragment
import kotlinx.android.synthetic.main.activity_register_layout.*
import kotlinx.android.synthetic.main.content_register_toolbar.*
import taxigoexpress.com.driver.R


class RegisterActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_register_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.loadView()
        this.configureToolbar()
//        this.initializeAWS()
    }


    private fun loadView() {
        this.replaceFragment(
            RegisterFragment.newInstance(),
            R.id.register_content,
            false
        )
    }

    private fun configureToolbar() {
        this.ivToolbarBack.clickEvent().observe(this, Observer {
            if (this.supportFragmentManager.backStackEntryCount > 0) {
                this.supportFragmentManager.popBackStack()
            } else {
                this.finish()
            }
        })
    }

    fun changeTitleToolbar(titleToolbar: String) {
        this.tvToolbarTitle.text = titleToolbar
    }

    fun hideToolbar() {
        this.register_appbar.gone()
    }

    fun showToolbar() {
        this.register_appbar.visible()
    }


}
