package taxigoexpress.com.driver.register.presentation.observers

import android.view.View
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.register.domain.exceptions.RegisterDriverException
import taxigoexpress.com.driver.register.presentation.liveData.RegisterLiveData
import java.io.IOException

class RegisterObserver(private val mRegisterLiveData: RegisterLiveData) :
    DefaultObserverLiveData<UserEntity>(mRegisterLiveData.registerObserver) {

    override fun onStart() {
        this.mRegisterLiveData.registerObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mRegisterLiveData.registerObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: UserEntity) {
        this.mRegisterLiveData.registerObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mRegisterLiveData.registerObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mRegisterLiveData.registerObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is RegisterDriverException -> useCase(error)
            else -> this.mRegisterLiveData.registerObserver.showExceptionObserver.value =
                error.localizedMessage
        }
    }

    private fun useCase(error: RegisterDriverException) {
        when (error.validationType) {
            RegisterDriverException.Type.NO_VALID_NAME -> this.mRegisterLiveData.nameErrorObserver.value =
                R.string.no_valid_name
            RegisterDriverException.Type.NO_VALID_LASTNAME -> this.mRegisterLiveData.lastNameErrorObserver.value =
                R.string.no_valid_lastname
            RegisterDriverException.Type.INVALID_PASSWORD -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.sign_in_empty_password_error
            RegisterDriverException.Type.NO_VALID_PHONE_NUMBER -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.no_valid_phone_number
            RegisterDriverException.Type.NO_VALID_EMAIL -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.sign_in_wrong_email_error
            RegisterDriverException.Type.UNCHECK_TC -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.uncheck_tc_message
        }
    }

}
