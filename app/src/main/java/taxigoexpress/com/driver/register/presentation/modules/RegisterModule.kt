package taxigoexpress.com.driver.register.presentation.modules

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import taxigoexpress.com.driver.register.data.repositoryImplementations.RegisterRepositoryImpl
import taxigoexpress.com.driver.register.data.services.RegisterServices
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.register.domain.repositoryAbstractions.RegisterRepository
import taxigoexpress.com.driver.register.presentation.viewmodels.abstractions.RegisterCarViewModel
import taxigoexpress.com.driver.register.presentation.viewmodels.abstractions.RegisterDriverViewModel
import taxigoexpress.com.driver.register.presentation.viewmodels.implementations.RegisterCarViewModelImpl
import taxigoexpress.com.driver.register.presentation.viewmodels.implementations.RegisterDriverViewModelImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import taxigoexpress.com.driver.login.data.repositoryImplementations.LoginRepositoryImpl
import taxigoexpress.com.driver.login.data.services.LoginServices
import taxigoexpress.com.driver.login.domain.repositoryAbstractions.LoginRepository
import javax.inject.Named

@Module
class RegisterModule {

    @Provides
    @FragmentScope
    fun providesRegisterServices(@Named("main_retrofit") retrofit: Retrofit): RegisterServices =
        retrofit.create(RegisterServices::class.java)

    @Provides
    @FragmentScope
    fun providesLoginServices(@Named("main_retrofit") retrofit: Retrofit): LoginServices =
        retrofit.create(LoginServices::class.java)

    @Provides
    @FragmentScope
    fun providesRegisterRepository(RegisterRepositoryImpl: RegisterRepositoryImpl): RegisterRepository =
        RegisterRepositoryImpl

    @Provides
    @FragmentScope
    fun providesLoginRepository(loginRepositoryImpl: LoginRepositoryImpl): LoginRepository =
        loginRepositoryImpl

    @Provides
    @FragmentScope
    fun providesRegisterViewmodel(registerViewModelImpl: RegisterDriverViewModelImpl): RegisterDriverViewModel<UserEntity> =
        registerViewModelImpl

    @Provides
    @FragmentScope
    fun providesRegisterCarViewmodel(registerCarViewModelImpl: RegisterCarViewModelImpl): RegisterCarViewModel<CarEntity> =
        registerCarViewModelImpl
}