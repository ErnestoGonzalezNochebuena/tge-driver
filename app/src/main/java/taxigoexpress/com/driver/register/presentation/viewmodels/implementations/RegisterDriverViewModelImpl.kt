package taxigoexpress.com.driver.register.presentation.viewmodels.implementations

import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.register.domain.useCases.RegisterDriverUseCase
import taxigoexpress.com.driver.register.presentation.liveData.RegisterLiveData
import taxigoexpress.com.driver.register.presentation.observers.RegisterObserver
import taxigoexpress.com.driver.register.presentation.viewmodels.abstractions.RegisterDriverViewModel
import javax.inject.Inject

class RegisterDriverViewModelImpl
@Inject constructor(
    private val registerDriverUseCase: RegisterDriverUseCase
) : BaseViewModelLiveData<UserEntity>(), RegisterDriverViewModel<UserEntity> {

    private val mRegisterLiveData by lazy { RegisterLiveData(this.getCustomLiveData()) }

    override fun register(request: UserEntity) {
        this.registerDriverUseCase.execute(
            RegisterObserver(mRegisterLiveData),
            request
        )
    }

    override fun observeRegister(): RegisterLiveData =
        this.mRegisterLiveData

    override fun observeResult(): BaseLiveData<UserEntity> =
        this.mRegisterLiveData.registerObserver

    override fun onCleared() {
        this.registerDriverUseCase.dispose()
        super.onCleared()
    }
}