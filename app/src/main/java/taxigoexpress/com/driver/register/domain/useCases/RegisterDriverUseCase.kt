package taxigoexpress.com.driver.register.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidEmail
import com.taxigoexpress.core.presentation.utils.isValidPassword
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.register.domain.exceptions.RegisterDriverException
import taxigoexpress.com.driver.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import taxigoexpress.com.driver.login.domain.repositoryAbstractions.LoginRepository
import javax.inject.Inject

class RegisterDriverUseCase
@Inject constructor(
    private val registerRepository: RegisterRepository,
    private val loginRepository: LoginRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<UserEntity, UserEntity>(threadExecutor, postExecutionThread) {


    override fun createObservable(params: UserEntity): Observable<UserEntity> = when {
        params.name.length <= 2 -> Observable.error(RegisterDriverException(RegisterDriverException.Type.NO_VALID_NAME))
        params.lastName.length <= 2 -> Observable.error(
            RegisterDriverException(
                RegisterDriverException.Type.NO_VALID_LASTNAME
            )
        )
        !params.password.isValidPassword() -> Observable.error(
            RegisterDriverException(
                RegisterDriverException.Type.INVALID_PASSWORD
            )
        )
        !params.email.isValidEmail() -> Observable.error(
            RegisterDriverException(
                RegisterDriverException.Type.NO_VALID_EMAIL
            )
        )
        params.phoneNumber.length < 10 -> Observable.error(
            RegisterDriverException(
                RegisterDriverException.Type.NO_VALID_PHONE_NUMBER
            )
        )
        params.phoneNumber.length < 10 -> Observable.error(
            RegisterDriverException(
                RegisterDriverException.Type.NO_VALID_PHONE_NUMBER
            )
        )
        !params.isAcceptedTC -> Observable.error(RegisterDriverException(RegisterDriverException.Type.UNCHECK_TC))
        else -> this.registerRepository.registerUser(params).flatMap { user ->
            this.registerRepository.registerDriver(
                this.setDataToDriver(
                    user,
                    params.password,
                    params.licenseNumber,
                    params.permission
                )
            ).flatMap {driver ->
                this.loginRepository.generateDriverRequest(driverId = driver._id.toString()).flatMap {
                    user._id = driver._id
                    Observable.just(user)
                }
            }
        }
    }

    private fun setDataToDriver(
        userEntity: UserEntity,
        password: String,
        licenseNumber: String,
        permission: String
    )
            : UserEntity {
        return UserEntity(
            userEntity.id,
            userEntity.name,
            userEntity.lastName,
            userEntity.phoneNumber,
            userEntity.email,
            password,
            5.0,
            userEntity.createdDate,
            permission,
            licenseNumber
        )
    }

}