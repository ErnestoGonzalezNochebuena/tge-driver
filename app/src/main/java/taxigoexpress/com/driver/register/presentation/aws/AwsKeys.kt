package taxigoexpress.com.driver.register.presentation.aws

object AwsKeys {

    const val DOC_TRACKING = "document.tracking"
    const val IDENTITY_POOL = "us-east-2:2d2c3da5-5e90-4b7d-b937-a6703018d097"
    const val BUCKET_NAME = "taxigodocumentos"
}