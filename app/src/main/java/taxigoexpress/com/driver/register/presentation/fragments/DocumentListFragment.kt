package taxigoexpress.com.driver.register.presentation.fragments

import android.os.Bundle
import android.view.View
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_document_list.*
import taxigoexpress.com.driver.HomeActivity
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.register.presentation.adapters.DocumentAdapter
import taxigoexpress.com.driver.register.presentation.adapters.entities.DocumentItem
import taxigoexpress.com.driver.register.presentation.aws.Document

class DocumentListFragment : BaseFragment() {

    private val mPreferencesManager by lazy {
        SharedPreferencesManager(requireContext())
    }

    override fun getLayout(): Int = R.layout.fragment_document_list

    override fun initView(view: View, savedInstanceState: Bundle?) {
        configureToolbar()
        showDocuments()
    }

    private fun getDocumentList(): List<DocumentItem> = mutableListOf(
        DocumentItem(
            Document.License,
            mPreferencesManager.getSharedPreference(Document.License.name, false)
        ),
        DocumentItem(
            Document.InsurancePolicy,
            mPreferencesManager.getSharedPreference(Document.InsurancePolicy.name, false)
        ),
        DocumentItem(
            Document.AddressDocument,
            mPreferencesManager.getSharedPreference(Document.AddressDocument.name, false)
        ),
        DocumentItem(
            Document.CardCirculation,
            mPreferencesManager.getSharedPreference(Document.CardCirculation.name, false)
        )
    )

    private fun showDocuments() {
        val documents = getDocumentList()
        document_list_rv.adapter = DocumentAdapter(documents, ::onSelectDocument)
    }

    private fun configureToolbar() {
        val homeActivity = this.activity as HomeActivity
        homeActivity.showSectionToolbar()
        homeActivity.changeTitleToolbar("Documentos")
    }

    private fun onSelectDocument(documentItem: DocumentItem) {
        replaceFragment(
            DocumentsFragment.newInstance(documentItem.type),
            R.id.home_container,
            true
        )
    }

}