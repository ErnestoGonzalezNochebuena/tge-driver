package taxigoexpress.com.driver.register.presentation.liveData

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData

data class RegisterCarLiveData(
    val registerCarObserver: BaseLiveData<CarEntity>,
    val placesErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val modelErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val colorErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val carRegisterObserver: MutableLiveData<Int> = MutableLiveData()
) {

}
