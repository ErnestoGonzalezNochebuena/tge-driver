package taxigoexpress.com.driver.register.presentation.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.managers.PhotoManager
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import kotlinx.android.synthetic.main.fragment_documents_layout.*
import taxigoexpress.com.driver.BuildConfig
import taxigoexpress.com.driver.HomeActivity
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.register.presentation.aws.AwsCore
import taxigoexpress.com.driver.register.presentation.aws.Document
import java.io.File
import java.lang.Exception

class DocumentsFragment : BaseFragment(), AwsCore.UploadCallback,
    PhotoManager.PhotoManagerCallback {

    private val mPreferencesManager by lazy {
        SharedPreferencesManager(requireContext())
    }

    private lateinit var file: File
    private val awsCore: AwsCore by lazy {
        AwsCore(requireActivity().applicationContext)
    }
    private lateinit var documentType: Document
    private val photoManager: PhotoManager by lazy {
        PhotoManager(this, this)
    }

    private val loaderDialog: LoaderFragment by lazy {
        LoaderFragment()
    }

    override fun getLayout(): Int = R.layout.fragment_documents_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadArguments()
        this.setClickListeners()
        this.configureToolbar()
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.documentType = it.getSerializable(DOCUMENT_TYPE) as Document
        }
    }

    private fun configureToolbar() {
        val homeActivity = this.activity as HomeActivity
        homeActivity.showSectionToolbar()
        homeActivity.changeTitleToolbar("Sube tus documentos")

        tvLoadDocuments.text = documentType.desc
    }

    private fun setClickListeners() {
        this.btnNextDocument.clickEvent().observe(this, Observer {
            this.awsCore.uploadDocument(
                this.documentType,
                this.file,
                this.generateDocumentPath(),
                this
            )
        })

        this.btnTryAgain.clickEvent().observe(this, Observer {
            this.openCamera()
        })

        this.clAddDocument.clickEvent().observe(this, Observer {
            this.openCamera()
        })
    }

    private fun generateDocumentPath(): String =
        this.keyStoreManager?.retrieveDataWithSafeMode(Constants.DRIVER_ID)?.let { idUser ->
            val env = if (BuildConfig.DEBUG) "debug_" else ""
            env + idUser + "_" + documentType.name + ".jpg"
        } ?: run {
            ""
        }


    private fun openCamera() {
        photoManager.openCamera()
    }

    override fun onStartUpload() {
        loaderDialog.show(childFragmentManager, javaClass.simpleName)
    }

    override fun onSuccessUpload(document: Document) {
        loaderDialog.dismiss()
        mPreferencesManager.setSharedPreference(document.name, true)
        showAlertWithAction("¡El documento se ha subido exitosamente!") {
            requireActivity().onBackPressed()
            true
        }
    }

    override fun onGetCurrentPercentage(percentage: Int) {

    }

    override fun onUploadFailed(document: Document, reason: String) {
        loaderDialog.dismiss()
        showAlert(reason)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            photoManager.onActivityResult(requestCode, resultCode, data)
        } catch (e: Exception) {
            this.showAlert(e.localizedMessage ?: "Error")
        }
    }

    private fun showPreview(mBitmap: Bitmap) {
        this.ivDocumentPreview.setImageBitmap(mBitmap)
        this.ivDocumentPreview.visible()
        this.ivDocumentPreview.bringToFront()
        this.btnTryAgain.visible()
        this.btnNextDocument.isEnabled = true
    }

    companion object {
        private const val DOCUMENT_TYPE = "document.type"

        fun newInstance(document: Document) = DocumentsFragment().apply {
            arguments = bundleOf(
                DOCUMENT_TYPE to document
            )
        }
    }

    override fun onGetPhotoManagerResult(bitmap: Bitmap, file: File) {
        showPreview(bitmap)
        this.file = file
    }
}
