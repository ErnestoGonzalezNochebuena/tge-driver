package taxigoexpress.com.driver.register.presentation.adapters

import android.content.res.ColorStateList
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.inflate
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.presentation.utils.clickEventObservable
import kotlinx.android.synthetic.main.item_document.view.*
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.register.presentation.adapters.entities.DocumentItem

class DocumentAdapter(
    private val data: List<DocumentItem>,
    private val callback: (item: DocumentItem) -> (Unit)
) : RecyclerView.Adapter<DocumentAdapter.ItemViewHolder>() {

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(parent.inflate(R.layout.item_document))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int): Unit = with(holder) {
        val document = data[position]
        tvName.text = document.type.desc
        tvStatus.text = document.status
        val color = if (document.isAdded) {
            imgCheck.visible()
            R.color.jungle_green
        } else {
            imgCheck.gone()
            R.color.alto_gray
        }

        ImageViewCompat.setImageTintList(
            imgCircle, ColorStateList.valueOf(
                ContextCompat.getColor(itemView.context, color)
            )
        )

        cvContainer.clickEventObservable().subscribe {
            callback(document)
        }
    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView = view.document_name
        val tvStatus: TextView = view.document_status
        val cvContainer: CardView = view.document_container
        val imgCircle: ImageView = view.document_status_circle
        val imgCheck: ImageView = view.document_status_check
    }

}