package taxigoexpress.com.driver.register.domain.datasourceAbstractions

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.register.domain.entities.KeysEntity
import io.reactivex.Observable

interface RegisterDataSource {
    fun registerUser(request: UserEntity): Observable<UserEntity>
    fun registerDriver(userId: String, request: UserEntity): Observable<UserEntity>
    fun registerCar(params: CarEntity): Observable<CarEntity>
    fun getAwsKeys(): Observable<KeysEntity>

}
