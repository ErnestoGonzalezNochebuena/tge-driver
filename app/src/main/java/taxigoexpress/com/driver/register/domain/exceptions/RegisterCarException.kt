package taxigoexpress.com.driver.register.domain.exceptions

class RegisterCarException
@JvmOverloads constructor(
    val validationType: Type,
    message: String? = "Register ${validationType.name}",
    cause: Throwable? = null
) :
    Throwable(message, cause) {

    enum class Type {
        NO_VALID_PLACES,
        NO_VALID_COLOR,
        INVALID_MODEL,
        NO_VALID_REGISTER
    }
}
