package taxigoexpress.com.driver.register.domain.entities

class DocumentsTrackingEntity {
    var licenseCompleted: Boolean = false
    var addressFormCompleted: Boolean = false
    var policyInsuranceCompleted: Boolean = false
    var idDocumentCompleted: Boolean = false
    var cardCirculation: Boolean = false
    var photoCompleted: Boolean = false
    var taxiCardCompleted: Boolean = false
}