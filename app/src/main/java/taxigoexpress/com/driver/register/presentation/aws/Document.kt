package taxigoexpress.com.driver.register.presentation.aws

import java.io.Serializable

enum class Document(val desc: String) : Serializable {
    License("Licencia de conducir"),
    InsurancePolicy("Póliza de seguro"),
    AddressDocument("Comprobante de domicilio"),
    CardCirculation("Tarjeta de circulación")
}