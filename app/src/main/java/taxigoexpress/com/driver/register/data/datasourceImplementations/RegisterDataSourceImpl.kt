package taxigoexpress.com.driver.register.data.datasourceImplementations

import com.taxigoexpress.core.domain.entities.CarEntity
import taxigoexpress.com.driver.register.data.services.RegisterServices
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.managers.ResponseManager
import taxigoexpress.com.driver.register.domain.datasourceAbstractions.RegisterDataSource
import taxigoexpress.com.driver.register.domain.entities.KeysEntity
import io.reactivex.Observable
import javax.inject.Inject

class RegisterDataSourceImpl
@Inject constructor(private val service: RegisterServices) : RegisterDataSource {

    override fun registerUser(request: UserEntity): Observable<UserEntity> =
        this.service.register(request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(ResponseManager.processWrongBody(it))
            }
        }

    override fun registerDriver(userId: String, request: UserEntity): Observable<UserEntity> =
        this.service.registerDriver(userId, request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(ResponseManager.processWrongBody(it))
            }
        }

    override fun registerCar(params: CarEntity): Observable<CarEntity> =
        this.service.registerCar(params.idCond, params).flatMap {
            if (it.isSuccessful) {
                Observable.just(params)
            } else {
                Observable.error(ResponseManager.processWrongBody(it))
            }
        }

    override fun getAwsKeys(): Observable<KeysEntity> =
        this.service.getAwsKeys().flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body()?.first())
            } else {
                Observable.error(ResponseManager.processWrongBody(it))
            }
        }
}
