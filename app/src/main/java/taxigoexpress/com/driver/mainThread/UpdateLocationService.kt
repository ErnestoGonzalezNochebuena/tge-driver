package taxigoexpress.com.driver.mainThread

import com.taxigoexpress.core.domain.entities.UserEntity
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface UpdateLocationService {
    @PUT("conductor/{id}")
    suspend fun updateDriver(
        @Path(value = "id") driverId: Int,
        @Body driver: UserEntity
    ): Deferred<UserEntity>

}