package taxigoexpress.com.driver.mainThread

import android.app.*
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.annotation.Nullable
import androidx.core.app.NotificationCompat
import com.google.gson.Gson
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import com.ia.mchaveza.kotlin_library.TrackingManagerLocationCallback
import com.taxigoexpress.core.R
import com.taxigoexpress.core.domain.ApiRegisterDriverService
import com.taxigoexpress.core.domain.ApiUtils
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.authentication.services.KeyStoreManager
import com.taxigoexpress.core.presentation.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import taxigoexpress.com.driver.HomeActivity


class SendLocationServices : Service(),
    LocationListener ,
    TrackingManagerLocationCallback {


    private var locationManager: LocationManager? = null
    private var mAPIService: ApiRegisterDriverService? = ApiUtils.apiRegisterDriverService
    private val preferencesManager by lazy { SharedPreferencesManager(this) }
    private val keyStoreManager by lazy { KeyStoreManager(this, preferencesManager) }
    private var currentDriver: UserEntity? = null
    private var currentId: String? = null


    private val trackingManager: TrackingManager by lazy {
        TrackingManager(this)
    }

    override fun onCreate() {
        super.onCreate()

        this.currentDriver = Gson().fromJson(
            this.keyStoreManager.retrieveDataWithSafeMode(Constants.DRIVER_INFO),
            UserEntity::class.java
        )

        this.currentDriver?.let {
            this.currentId = it.id.toString()
        }

        Log.d("GHP", "User: $currentDriver")

        this.locationManager = getSystemService(LOCATION_SERVICE) as? LocationManager

        try {
            this.locationManager?.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                0L,
                0f,
                this
            )

            this.locationManager?.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                0,
                0f,
                this
            )
        } catch (ex: SecurityException) {
            Log.d("GHP", "Security Exception, no location available")
        }

        this.trackingManager.startLocationUpdates(
            this,
            6000,
            6000
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        this.locationManager?.removeUpdates(this)
    }

    private fun sendValueLocation(request: UserEntity) {

        Log.d("GHP", "User: $currentDriver")

        currentId?.let { id ->
            this.mAPIService?.updateRegisterDriver(id, request)
                ?.enqueue(object : Callback<TripEntity?> {
                    override fun onFailure(call: Call<TripEntity?>, t: Throwable) {
                        Log.d("GHP", "onFailure ----> $call")
                    }

                    override fun onResponse(
                        call: Call<TripEntity?>,
                        response: Response<TripEntity?>
                    ) {
                        Log.d("GHP", "onResponse ----> $call")
                    }

                })
        }
    }

    override fun onLocationChanged(location: Location?) {
        Log.d(
            "GHP",
            "onLocationChanged ----> Long: ${location?.longitude} :  Lat: ${location?.latitude}"
        )
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        // Este metodo se ejecuta cada vez que se detecta un cambio en el
        // status del proveedor de localizaci—n (GPS)
        // Los diferentes Status son:
        // OUT_OF_SERVICE -> Si el proveedor esta fuera de servicio
        // TEMPORARILY_UNAVAILABLE -> Temporalmente no disponible pero se
        // espera que este disponible en breve
        // AVAILABLE -> Disponible
        return
    }

    override fun onProviderEnabled(provider: String?) {
        // GPS Activado
        return
    }

    override fun onProviderDisabled(provider: String?) {
        // GPS Desactivado
        return
    }

    override fun onLocationHasChanged(location: Location) {
        currentDriver?.let {
            it.currentLatitude = location.latitude
            it.currentLength = location.longitude
            this.sendValueLocation(it)
        }

        this.locationManager?.removeUpdates(this)
    }

    override fun onLocationHasChangedError(error: Exception) {

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        createNotificationChannel()
        val notificationIntent = Intent(this, HomeActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification: Notification =
            NotificationCompat.Builder(this,
                CHANNEL_ID
            )
                .setContentTitle("Taxi Go Express")
                .setContentText("En servicio")
                .setSmallIcon(R.drawable.ic_car_activate_service)
                .setContentIntent(pendingIntent)
                .build()
        startForeground(1, notification)
        //do heavy work on a background thread
        //stopSelf();
        return START_NOT_STICKY
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }


    @Nullable
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    companion object{
        const val CHANNEL_ID = "ForegroundServiceChannel"
    }
}