package taxigoexpress.com.driver.mainThread

import com.taxigoexpress.core.domain.entities.UserEntity

class UpdateLocationRepository {
    var client: UpdateLocationService = RetrofitClient.retrofit
    suspend fun updateDriver(driverId: Int, driver: UserEntity) =
        client.updateDriver(driverId, driver)
}
