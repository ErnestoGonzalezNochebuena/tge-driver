package taxigoexpress.com.driver.mainThread

import com.google.gson.GsonBuilder
import com.taxigoexpress.core.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://50.21.189.124:8080")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(UpdateLocationService::class.java)
}