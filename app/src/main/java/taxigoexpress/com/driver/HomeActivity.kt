package taxigoexpress.com.driver

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import taxigoexpress.com.driver.login.presentation.activities.LoginActivity
import kotlinx.android.synthetic.main.activity_home_layout.*
import kotlinx.android.synthetic.main.content_home_toolbar.*
import kotlinx.android.synthetic.main.navigation_view.*
import kotlinx.android.synthetic.main.navigation_view_menu.*
import org.jetbrains.anko.intentFor
import taxigoexpress.com.driver.mainThread.SendLocationServices
import taxigoexpress.com.driver.register.presentation.fragments.DocumentListFragment
import taxigoexpress.com.driver.statistics.presentation.fragments.CurrenciesFragment
import taxigoexpress.com.driver.statistics.presentation.fragments.StadisticsFragment
import taxigoexpress.com.driver.trips.presentation.adapters.MainMenuListener
import taxigoexpress.com.driver.trips.presentation.adapters.OptionsMenuAdapter
import taxigoexpress.com.driver.trips.presentation.fragments.MainTripViewFragment
import taxigoexpress.com.driver.trips.presentation.fragments.TripsHistoryFragment

class HomeActivity : BaseActivity() {

    private val CHANNEL_DEFAULT_IMPORTANCE = "001"

    override fun getLayoutResId(): Int = R.layout.activity_home_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.launchMainView()
        this.configureMenu()
    }

    fun configureFCM(isActive: Boolean) {
        val topicActive = if (isActive) {
            "tge"
        } else {
            "inactiveUser"
        }
        val topicInactive = if (!isActive) {
            "tge"
        } else {
            "inactiveUser"
        }
        FirebaseMessaging.getInstance().subscribeToTopic(topicActive)
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topicInactive)
            .addOnCompleteListener { task ->
                var msg = getString(R.string.msg_subscribe_failed)
                if (task.isSuccessful) {
                    msg = getString(R.string.msg_subscribed)
                }
//                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            }
    }


    private fun configureMenu() {
        setSupportActionBar(home_toolbar)
        this.replaceFragment(MainTripViewFragment.newInstance(), R.id.home_container, false)
        val currentDriver = Gson().fromJson(
            this.keyStoreManager.retrieveDataWithSafeMode(Constants.DRIVER_INFO),
            UserEntity::class.java
        )
        if (currentDriver == null) {
            this.startActivity(intentFor<LoginActivity>())
            this.finish()
        }
    }

    fun startService() {
        val serviceIntent = Intent(this, SendLocationServices::class.java)
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android")
        ContextCompat.startForegroundService(this, serviceIntent)
        preferencesManager.setSharedPreference(Constants.ONLINE, true)
    }

    fun stopService() {
        val serviceIntent = Intent(this, SendLocationServices::class.java)
        stopService(serviceIntent)
        preferencesManager.setSharedPreference(Constants.ONLINE, false)
    }

    private fun launchMainView() {
        val currentDriver = Gson().fromJson(
            this.keyStoreManager.retrieveDataWithSafeMode(Constants.DRIVER_INFO),
            UserEntity::class.java
        )

        val currentAuto = Gson().fromJson(
            this.keyStoreManager.retrieveDataWithSafeMode(Constants.CAR_INFO),
            CarEntity::class.java
        )

        this.ivToolbarMenu.clickEvent().observe(this, Observer {
            currentDriver?.let {
                this.tvUsername.text =
                    String.format("%s %s", currentDriver.name, currentDriver.lastName)
            }
            this.drawer_layout.openDrawer(Gravity.LEFT)
        })

        currentDriver?.let {
            this.tvToolbarUsername.text =
                String.format("%s %s", currentDriver.name, currentDriver.lastName)
        }

        currentAuto?.let {
            this.tvToolbarContent.text =
                String.format("%s %s", currentAuto._brandCar, currentAuto._model)
        }


        /*this.ivNavigationViewMenu.clickEvent().observe(this, Observer {
            this.drawer_layout.closeDrawer(Gravity.LEFT)
        })*/

        val adapter = OptionsMenuAdapter(this.resources.getStringArray(R.array.menu))
        this.rvSideMenu.adapter = adapter
        this.rvSideMenu.layoutManager = LinearLayoutManager(this)
        adapter.setListener(object : MainMenuListener {
            override fun onSelectOption(option: MainMenuListener.OptionsMenu) {
                this@HomeActivity.drawer_layout.closeDrawer(Gravity.LEFT)
                var fragment: Fragment? = null
                when (option) {
                    MainMenuListener.OptionsMenu.TripsHistory -> fragment =
                        StadisticsFragment.newInstance()
                    MainMenuListener.OptionsMenu.Stadistics -> fragment =
                        TripsHistoryFragment.newInstance()
                    MainMenuListener.OptionsMenu.Curreny -> fragment =
                        CurrenciesFragment.newInstance()
                    MainMenuListener.OptionsMenu.Documents -> fragment =
                        DocumentListFragment()
                    else -> this@HomeActivity.closeSession()
                }
                fragment?.let {
                    this@HomeActivity.replaceFragment(fragment, R.id.home_container, true)
                }
            }
        })

        this.clNavigationView.setBackgroundColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )
    }

    private fun closeSession() {
        this.keyStoreManager.clearData(Constants.DRIVER_INFO)
        this.keyStoreManager.clearData(Constants.PASS)
        this.keyStoreManager.clearData(Constants.PASSWORD)
        this.keyStoreManager.clearData(Constants.CAR_INFO)
        this.keyStoreManager.clearData(Constants.DRIVER_ID)
        this.keyStoreManager.clearData(Constants.CURRENT_TRIP)
        this.startActivity(intentFor<LoginActivity>())
        this.configureFCM(false)
        this.stopService()
        this.preferencesManager.setSharedPreference(Constants.ONLINE, false)
        this.finish()
    }

    private fun configureToolbar() {
        this.ivToolbarBack.clickEvent().observe(this, Observer {
            if (this.supportFragmentManager.backStackEntryCount > 0) {
                this.supportFragmentManager.popBackStack()
            } else {
                this.finish()
            }
        })
    }

    fun changeTitleToolbar(titleToolbar: String) {
        this.tvToolbarTitle.text = titleToolbar
    }

    fun setUserInformationToolbar(username: String, content: String) {
        this.tvToolbarUsername.text = username
        this.tvToolbarContent.text = content
    }

    fun showMainToolbar() {
        this.ivToolbarMenu.visible()
        this.llUserInformationTitle.visible()
        this.ivImageToolbar.visible()
        this.ivToolbarBack.gone()
        this.tvToolbarTitle.gone()
    }

    fun showSectionToolbar() {
        this.ivToolbarMenu.gone()
        this.llUserInformationTitle.gone()
        this.ivImageToolbar.gone()
        this.ivToolbarBack.visible()
        this.tvToolbarTitle.visible()
    }

}
