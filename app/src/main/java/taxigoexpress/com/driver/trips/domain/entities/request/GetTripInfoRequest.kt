package taxigoexpress.com.driver.trips.domain.entities.request

data class GetTripInfoRequest(
    val tripId: String
)