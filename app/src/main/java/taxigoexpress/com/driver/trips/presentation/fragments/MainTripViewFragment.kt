package taxigoexpress.com.driver.trips.presentation.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.ia.mchaveza.kotlin_library.*
import com.ncorti.slidetoact.SlideToActView
import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import taxigoexpress.com.driver.trips.presentation.activities.TripActivity
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.Constants
import kotlinx.android.synthetic.main.fragment_main_trip_view_layout.*
import org.jetbrains.anko.support.v4.intentFor
import taxigoexpress.com.driver.HomeActivity
import taxigoexpress.com.driver.R


/**
 * A simple [Fragment] subclass.
 */
class MainTripViewFragment : BaseFragment(),
    OnMapReadyCallback,
    PermissionCallback,
    TrackingManagerLocationCallback {

    private var mGoogleMap: GoogleMap? = null
    private var currentLocationFounded: Location? = null

    private val trackingManager: TrackingManager by lazy {
        TrackingManager(this.requireContext())
    }

    private val permissionManager by lazy { PermissionManager(requireActivity(), this) }

    override fun getLayout(): Int = R.layout.fragment_main_trip_view_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        (this.activity as HomeActivity).showMainToolbar()
        this.mvTripPreview.onCreate(savedInstanceState)
        this.mvTripPreview.getMapAsync(this)
        this.configureButtons()
        this.configureToolbar()
//        if (this.keyStoreManager!!.retrieveDataWithSafeMode(Constants.DRIVER_INFO).isEmpty()) {
//            this.requireActivity().finish()
//        }
    }

    private fun configureToolbar() {
        val currentDriver = Gson().fromJson(
            this.keyStoreManager?.retrieveDataWithSafeMode(Constants.DRIVER_INFO),
            UserEntity::class.java
        )
        val currentVehicle = Gson().fromJson(
            this.keyStoreManager?.retrieveDataWithSafeMode(Constants.CAR_INFO),
            CarEntity::class.java
        )

        currentDriver?.let { driver ->
            currentVehicle?.let { car ->
                (this.requireActivity() as HomeActivity).setUserInformationToolbar(
                    "${driver.name} ${driver.lastName}",
                    "${car._brandCar} ${car._model}"
                )
            }
        }

        Gson().fromJson(
            this.preferencesManager?.getSharedPreference(
                Constants.CURRENT_TRIP,
                ""
            ),
            TripEntity::class.java
        )?.let {
            this.activity?.startActivity(intentFor<TripActivity>())
        }
    }

    private fun configureButtons() {
        if (preferencesManager!!.getSharedPreference(Constants.ONLINE, false)) {
            this.savInitConnection.gone()
            this.savEndConnection.visible()
        } else {
            this.savInitConnection.visible()
            this.savEndConnection.gone()
        }
        this.savInitConnection.onSlideCompleteListener =
            object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    this@MainTripViewFragment.savInitConnection.gone()
                    this@MainTripViewFragment.savEndConnection.visible()
                    this@MainTripViewFragment.savInitConnection.resetSlider()
                    (this@MainTripViewFragment.activity as HomeActivity).startService()
                    (this@MainTripViewFragment.activity as HomeActivity).configureFCM(true)
                }

            }

        this.savEndConnection.onSlideCompleteListener =
            object : SlideToActView.OnSlideCompleteListener {
                override fun onSlideComplete(view: SlideToActView) {
                    this@MainTripViewFragment.savEndConnection.gone()
                    this@MainTripViewFragment.savInitConnection.visible()
                    this@MainTripViewFragment.savEndConnection.resetSlider()
                    (this@MainTripViewFragment.activity as HomeActivity).stopService()
                    (this@MainTripViewFragment.activity as HomeActivity).configureFCM(false)

                }
            }
    }


    companion object {
        fun newInstance(): MainTripViewFragment {
            return MainTripViewFragment()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        this.mGoogleMap = p0
        this.mGoogleMap?.isMyLocationEnabled = false
        this.mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = false
        this.setCurrentLocationOnMap()
    }

    private fun setCurrentLocationOnMap() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!this.permissionManager.requestSinglePermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.startLocating()
            }
        } else {
            if (permissionManager.permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.startLocating()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocating() {
        this.mGoogleMap?.isMyLocationEnabled = true
        this.mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = true
        this.trackingManager.startLocationUpdates(
            this,
            5000,
            5000
        )

    }

    override fun onPermissionDenied(permission: String) {
    }

    override fun onPermissionGranted(permission: String) {
        this.setCurrentLocationOnMap()
    }

    override fun onLocationHasChanged(location: Location) {
        this.currentLocationFounded?.let {
            this.addMarkerToMap(location)
        } ?: kotlin.run {
            this.addMarkerToMap(location)
        }
        this.currentLocationFounded = location
    }

    override fun onLocationHasChangedError(error: Exception) {
        this.trackingManager.stopLocationUpdates()
    }

    private fun addMarkerToMap(location: Location) {
        this.mGoogleMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(location.latitude, location.longitude),
                15f
            )
        )
        this.mGoogleMap?.animateCamera(CameraUpdateFactory.zoomTo(15f))
    }

    override fun onResume() {
        super.onResume()
        this.mvTripPreview?.onResume()
    }

    override fun onPause() {
        super.onPause()
        this.mvTripPreview?.onPause()
    }

    override fun onStart() {
        super.onStart()
        this.mvTripPreview?.onStart()
    }

    override fun onStop() {
        super.onStop()
        this.mvTripPreview?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.mvTripPreview?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        this.mvTripPreview?.onLowMemory()
    }

}
