package taxigoexpress.com.driver.trips.domain.usecases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.domain.entities.request.GetTripInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripInfoResponse
import taxigoexpress.com.driver.trips.domain.repository.TripRepository
import javax.inject.Inject

class CheckTripAvailabilityUseCase @Inject constructor(
    private val tripRepository: TripRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<TripInfoResponse, GetTripInfoRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: GetTripInfoRequest): Observable<TripInfoResponse> =
        tripRepository.getTripInfo(params)

}