package taxigoexpress.com.driver.trips.domain.entities.request

enum class TripStatus(val desc: String) {
    FIRST("pedido"),
    WAITING("esperando"),
    STARTED("iniciado"),
    IN_PROCESS("en proceso"),
    FINISHED("finalizado"),
    CANCELLED("cancelado")
}