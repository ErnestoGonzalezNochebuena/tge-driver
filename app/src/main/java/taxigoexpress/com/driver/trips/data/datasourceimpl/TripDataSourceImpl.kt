package taxigoexpress.com.driver.trips.data.datasourceimpl

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.managers.ResponseManager.handleResponse
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.data.services.TripService
import taxigoexpress.com.driver.trips.domain.datasource.TripDataSource
import taxigoexpress.com.driver.trips.domain.entities.request.AssignDriverToTripRequest
import taxigoexpress.com.driver.trips.domain.entities.request.GetTripInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.request.GetCustomerInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripInfoResponse
import javax.inject.Inject

class TripDataSourceImpl @Inject constructor(
    private val service: TripService
) : TripDataSource {
    override fun assignDriverToTrip(request: AssignDriverToTripRequest): Observable<TripEntity> =
        service.assignDriverToTrip(request.tripId, request.driverId, request.tripEntity).flatMap {
            it.handleResponse()
        }

    override fun updateTripStatus(request: UpdateTripStatusRequest): Observable<TripEntity> =
        service.updateTrip(request.tripId, request.status.desc, request.tripEntity).flatMap {
            it.handleResponse()
        }

    override fun getTripInfo(request: GetTripInfoRequest): Observable<TripInfoResponse> =
        service.getTripInfo(request.tripId).flatMap {
            it.handleResponse()
        }

    override fun getCustomerInfo(request: GetCustomerInfoRequest): Observable<UserEntity> =
        service.getCustomerInfo(request.customerId).flatMap {
            it.handleResponse()
        }


}