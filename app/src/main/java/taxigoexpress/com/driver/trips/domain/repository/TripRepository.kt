package taxigoexpress.com.driver.trips.domain.repository

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.domain.entities.request.AssignDriverToTripRequest
import taxigoexpress.com.driver.trips.domain.entities.request.GetTripInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.request.GetCustomerInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripInfoResponse

interface TripRepository {
    fun assignDriverToTrip(request: AssignDriverToTripRequest): Observable<TripEntity>
    fun updateTripStatus(request: UpdateTripStatusRequest): Observable<TripEntity>
    fun getTripInfo(request: GetTripInfoRequest): Observable<TripInfoResponse>
    fun getCustomerInfo(request: GetCustomerInfoRequest): Observable<UserEntity>
}