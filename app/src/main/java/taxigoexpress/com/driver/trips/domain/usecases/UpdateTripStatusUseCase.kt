package taxigoexpress.com.driver.trips.domain.usecases

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.repository.TripRepository
import javax.inject.Inject

class UpdateTripStatusUseCase @Inject constructor(
    private val tripRepository: TripRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<TripEntity, UpdateTripStatusRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: UpdateTripStatusRequest): Observable<TripEntity> =
        tripRepository.updateTripStatus(params)

}