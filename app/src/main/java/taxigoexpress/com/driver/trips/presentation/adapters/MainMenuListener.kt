package taxigoexpress.com.driver.trips.presentation.adapters

interface MainMenuListener {
    fun onSelectOption(option:OptionsMenu)


    enum class OptionsMenu {
        TripsHistory,
        Stadistics,
        Curreny,
        Profile,
        Documents
    }
}
