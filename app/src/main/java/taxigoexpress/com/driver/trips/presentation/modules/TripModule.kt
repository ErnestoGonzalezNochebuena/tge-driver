package taxigoexpress.com.driver.trips.presentation.modules

import com.taxigoexpress.core.presentation.scopes.FragmentScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import taxigoexpress.com.driver.trips.data.repositoryimpl.TripRepositoryImpl
import taxigoexpress.com.driver.trips.data.services.TripService
import taxigoexpress.com.driver.trips.domain.repository.TripRepository
import taxigoexpress.com.driver.trips.presentation.viewmodels.abs.TripViewModel
import taxigoexpress.com.driver.trips.presentation.viewmodels.impl.TripViewModelImpl
import javax.inject.Named

@Module
class TripModule {

    @Provides
    @FragmentScope
    fun providesTripService(@Named("main_retrofit") retrofit: Retrofit): TripService =
        retrofit.create(TripService::class.java)

    @Provides
    @FragmentScope
    fun providesTripRepository(repository: TripRepositoryImpl): TripRepository = repository

    @Provides
    @FragmentScope
    fun providesTripViewModel(viewModel: TripViewModelImpl): TripViewModel = viewModel
}