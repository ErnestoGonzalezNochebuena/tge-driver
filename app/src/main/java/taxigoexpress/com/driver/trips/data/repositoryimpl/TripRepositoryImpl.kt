package taxigoexpress.com.driver.trips.data.repositoryimpl

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.data.datasourceimpl.TripDataSourceImpl
import taxigoexpress.com.driver.trips.domain.entities.request.AssignDriverToTripRequest
import taxigoexpress.com.driver.trips.domain.entities.request.GetTripInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.request.GetCustomerInfoRequest
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripInfoResponse
import taxigoexpress.com.driver.trips.domain.repository.TripRepository
import javax.inject.Inject

class TripRepositoryImpl @Inject constructor(
    private val dataSource: TripDataSourceImpl
) : TripRepository {
    override fun assignDriverToTrip(request: AssignDriverToTripRequest): Observable<TripEntity> =
        dataSource.assignDriverToTrip(request)

    override fun updateTripStatus(request: UpdateTripStatusRequest): Observable<TripEntity> =
        dataSource.updateTripStatus(request)

    override fun getTripInfo(request: GetTripInfoRequest): Observable<TripInfoResponse> =
        dataSource.getTripInfo(request)

    override fun getCustomerInfo(request: GetCustomerInfoRequest): Observable<UserEntity> =
        dataSource.getCustomerInfo(request)

}