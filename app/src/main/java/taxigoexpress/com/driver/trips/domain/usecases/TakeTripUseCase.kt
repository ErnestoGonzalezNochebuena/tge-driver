package taxigoexpress.com.driver.trips.domain.usecases

import android.content.res.Resources
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.domain.entities.request.*
import taxigoexpress.com.driver.trips.domain.entities.response.TripAndUserEntity
import javax.inject.Inject

class TakeTripUseCase @Inject constructor(
    private val checkTripAvailabilityUseCase: CheckTripAvailabilityUseCase,
    private val getCustomerInfoUseCase: GetCustomerInfoUseCase,
    private val updateTripStatusUseCase: UpdateTripStatusUseCase,
    private val assignDriverToTripUseCase: AssignDriverToTripUseCase,
    private val resources: Resources,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<TripAndUserEntity, AssignDriverToTripRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: AssignDriverToTripRequest): Observable<TripAndUserEntity> {
        var tripEntity = TripEntity()
        return assignDriverToTripUseCase.createObservable(
            params
        ).flatMap {
            updateTripStatusUseCase.createObservable(
                UpdateTripStatusRequest(
                    it.id.toString(),
                    TripStatus.WAITING,
                    params.tripEntity
                )
            )
        }.flatMap {
            tripEntity = it
            checkTripAvailabilityUseCase.createObservable(
                GetTripInfoRequest(
                    params.tripId
                )
            )
        }.flatMap {
            tripEntity = tripEntity.copy(_id_pas = it.passId)
            getCustomerInfoUseCase.createObservable(
                GetCustomerInfoRequest(it.passId.toString())
            )
        }.flatMap {
            Observable.just(TripAndUserEntity(tripEntity, it))
        }
    }
}