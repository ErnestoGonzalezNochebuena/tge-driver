package taxigoexpress.com.driver.trips.domain.entities.request

import com.taxigoexpress.core.domain.entities.TripEntity

data class UpdateTripStatusRequest(
    val tripId: String,
    val status: TripStatus,
    val tripEntity: TripEntity
)