package taxigoexpress.com.driver.trips.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import dagger.Component
import taxigoexpress.com.driver.trips.presentation.fragments.CurrentTripFragment
import taxigoexpress.com.driver.trips.presentation.modules.TripModule

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [TripModule::class]
)
interface TripComponent {
    fun inject(fragment: CurrentTripFragment)
}