package taxigoexpress.com.driver.trips.domain.entities.response

import com.google.gson.annotations.SerializedName

data class TripInfoResponse(
    @SerializedName("marca")
    private val _brand: String? = null,
    @SerializedName("modelo")
    private val _model: String? = null,
    @SerializedName("color")
    private val _color: String? = null,
    @SerializedName("placas")
    private val _plateNumber: String? = null,
    @SerializedName("nombre")
    private val _name: String? = null,
    @SerializedName("rating")
    private val _rating: Double? = null,
    @SerializedName("pasId")
    private val _passId: Int? = null,
    @SerializedName("servicioId")
    private val _serviceId: Int? = null,
    @SerializedName("condId")
    private val _driverId: Int? = null
) {
    val brand: String get() = _brand ?: ""
    val model: String get() = _model ?: ""
    val color: String get() = _color ?: ""
    val plateNumber: String get() = _plateNumber ?: ""
    val name: String get() = _name ?: ""
    val rating: Double get() = _rating ?: 0.0
    val passId: Int get() = _passId ?: 0
    val serviceId: Int get() = _serviceId ?: 0
}