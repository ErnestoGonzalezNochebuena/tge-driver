package taxigoexpress.com.driver.trips.presentation.viewmodels.abs

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModel
import taxigoexpress.com.driver.trips.domain.entities.request.AssignDriverToTripRequest
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripAndUserEntity

abstract class TripViewModel : BaseViewModel() {
    abstract val updateTripStatusLiveData: BaseLiveData<TripEntity>
    abstract val takeTripLiveData: BaseLiveData<TripAndUserEntity>
    abstract fun updateTripStatus(request: UpdateTripStatusRequest)
    abstract fun takeTrip(request: AssignDriverToTripRequest)
}