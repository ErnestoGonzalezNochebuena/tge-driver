package taxigoexpress.com.driver.trips.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import taxigoexpress.com.driver.trips.domain.entities.response.TripInfoResponse

interface TripService {

    @PUT(BuildConfig.ADD_DRIVER_IN_TRIP)
    fun assignDriverToTrip(
        @Path("trip_id") tripId: String,
        @Path("id_cond") driverId: String,
        @Body tripEntity: TripEntity
    ): Observable<Response<TripEntity>>

    @PUT(BuildConfig.UPDATE_TRIP_ON_WAY)
    fun updateTrip(
        @Path("id") tripId: String,
        @Path("status") status: String,
        @Body tripEntity: TripEntity
    ): Observable<Response<TripEntity>>

    @GET(BuildConfig.GET_TRIP_CLIENT)
    fun getCustomerInfo(
        @Path("id") clientId: String
    ): Observable<Response<UserEntity>>

    @GET(BuildConfig.GET_TRIP_INFO)
    fun getTripInfo(
        @Path("trip_id") tripId: String
    ): Observable<Response<TripInfoResponse>>

}