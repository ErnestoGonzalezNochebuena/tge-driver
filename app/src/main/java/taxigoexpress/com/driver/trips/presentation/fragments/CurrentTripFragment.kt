package taxigoexpress.com.driver.trips.presentation.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper.getMainLooper
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.ia.mchaveza.kotlin_library.PermissionCallback
import com.ia.mchaveza.kotlin_library.PermissionManager
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.jakewharton.rxbinding.view.enabled
import com.mapbox.android.core.location.*
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode.TRACKING
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode.GPS
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.*
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.core.presentation.utils.createDialog
import kotlinx.android.synthetic.main.fragment_main_trip_layout.*
import org.jetbrains.anko.support.v4.intentFor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import taxigoexpress.com.driver.HomeActivity
import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.trips.domain.entities.request.AssignDriverToTripRequest
import taxigoexpress.com.driver.trips.domain.entities.request.TripStatus
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripAndUserEntity
import taxigoexpress.com.driver.trips.presentation.components.DaggerTripComponent
import taxigoexpress.com.driver.trips.presentation.components.TripComponent
import taxigoexpress.com.driver.trips.presentation.modules.TripModule
import taxigoexpress.com.driver.trips.presentation.viewmodels.abs.TripViewModel
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

class CurrentTripFragment : BaseFragment(),
    OnMapReadyCallback, PermissionCallback {

    private var tripEntity: TripEntity? = null

    private val loaderFragment: LoaderFragment by lazy {
        LoaderFragment()
    }

    private lateinit var map: MapboxMap
    private lateinit var originalLocation: Location
    private lateinit var originalPosition: Point
    private lateinit var destinationPoint: Point

    private val locationEngine: LocationEngine by lazy {
        LocationEngineProvider.getBestLocationEngine(requireContext())
    }

    private var locationLayerPlugin: LocationLayerPlugin? = null
    private var navigationMapRoute: NavigationMapRoute? = null
    private var status: TripStatus = TripStatus.WAITING

    private var destinationMarker: Marker? = null

    private val permissionManager by lazy { PermissionManager(requireActivity(), this) }

    @Inject
    lateinit var tripViewModel: TripViewModel

    private val driverId by lazy {
        keyStoreManager?.retrieveDataWithSafeMode(Constants.DRIVER_ID) ?: ""
    }

    private val assignDriverToTripRequest
        get() = AssignDriverToTripRequest(
            tripEntity?.id.toString(),
            driverId,
            tripEntity ?: TripEntity()
        )

    private val component: TripComponent by lazy {
        DaggerTripComponent.builder()
            .applicationComponent(requireActivity().getApplicationComponent())
            .tripModule(TripModule())
            .build()
    }

    private val updateTripRequest
        get() = UpdateTripStatusRequest(
            tripEntity?.id.toString(),
            status,
            tripEntity ?: TripEntity()
        )

    private val callback: LocationChangeListener by lazy {
        LocationChangeListener(this)
    }

    private var route: DirectionsRoute? = null

    private var userEntity: UserEntity? = null

    override fun getLayout(): Int = R.layout.fragment_main_trip_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(requireContext(), this.getString(R.string.mapbox_access_token))
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationEngine() {
        map.setStyle(Style.TRAFFIC_DAY) {
            enableLocationComponent(it)

            val request = LocationEngineRequest.Builder(1000L)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(1000L).build()

            locationEngine.requestLocationUpdates(request, callback, getMainLooper())
            locationEngine.getLastLocation(callback)
            //setCameraPosition(lastLocation)
        }
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationLayer() {
        this.locationLayerPlugin = LocationLayerPlugin(this.mapView, map, locationEngine)
        this.locationLayerPlugin?.setLocationLayerEnabled(true)
        this.locationLayerPlugin?.cameraMode = TRACKING
        this.locationLayerPlugin?.renderMode = GPS
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        val locationComponent: LocationComponent = map.locationComponent

        val locationComponentActivationOptions: LocationComponentActivationOptions =
            LocationComponentActivationOptions.builder(requireContext(), loadedMapStyle)
                .useDefaultLocationEngine(false)
                .build()

        locationComponent.activateLocationComponent(locationComponentActivationOptions)

        locationComponent.isLocationComponentEnabled = true

        locationComponent.cameraMode = CameraMode.TRACKING

        locationComponent.renderMode = RenderMode.GPS
    }

    private fun setCameraPosition(location: Location) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(location.latitude, location.longitude), 16.0
            )
        )
    }

    private fun setCameraPositionWithCoordenates(latLng: LatLng) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                latLng, 16.0
            )
        )
    }

    private fun setCameraPositionBounds(origin: LatLng, destination: LatLng) {
        val latLngBounds = LatLngBounds.Builder()
            .include(origin)
            .include(destination)
            .build()
        map.animateCamera(
            CameraUpdateFactory.newLatLngBounds(
                latLngBounds,
                50
            )
        )
    }

    private fun getRoute(originPoint: Point, destinationPoint: Point) {
        Mapbox.getAccessToken()?.let {
            NavigationRoute.builder(requireContext())
                .accessToken(it)
                .origin(originPoint)
                .destination(destinationPoint)
                .build()
                .getRoute(object : Callback<DirectionsResponse> {
                    override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                        Log.e("TripActivity", "Error ${t.message}")
                    }

                    override fun onResponse(
                        call: Call<DirectionsResponse>,
                        response: Response<DirectionsResponse>
                    ) {
                        val body = response.body() ?: return
                        if (body.routes().count() == 0) {
                            Log.e("TripActivity", "No routes founded")
                            return
                        }
                        showRoute(body.routes().first(), originPoint, destinationPoint)
                    }

                })
        }

    }

    private fun showRoute(directionRoute: DirectionsRoute, originPoint: Point, destinationPoint: Point) {
        navigationMapRoute?.let {
            navigationMapRoute?.removeRoute()
        } ?: kotlin.run {
            navigationMapRoute = NavigationMapRoute(null, mapView, map)
        }
        route = directionRoute
        navigationMapRoute?.addRoute(route)

        if (tripEntity?.status?.toLowerCase() == TripStatus.FIRST.desc) {
            setCameraPositionBounds(
                LatLng(originPoint.latitude(), originPoint.longitude()),
                LatLng(destinationPoint.latitude(), destinationPoint.longitude())
            )

            val distanceText = getDistanceText(directionRoute.distance() ?: 0.0)
            tvDistance.text = "Viaje a $distanceText"
            tvDistance.visible()
        } else {
            btnNavigate.enabled()
            setCameraPositionWithCoordenates(
                LatLng(originPoint.latitude(), originPoint.longitude())
            )
        }
    }

    private fun getDistanceText(meters: Double): String = if (meters >= 1000) {
        val km: Double = (meters / 1000)
        String.format("%.2f km.", km)
    } else {
        "$meters metros"
    }


    override fun initView(view: View, savedInstanceState: Bundle?) {
        component.inject(this)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        configureButtons()
        loadObservers()
    }

    private fun loadObservers() {
        with(tripViewModel.takeTripLiveData) {
            customObserver.observe(this@CurrentTripFragment, Observer {
                onTakeTripSuccess(it)
            })

            loadingObserver.observe(this@CurrentTripFragment, Observer {
                if (it == View.VISIBLE) {
                    loaderFragment.show(childFragmentManager, javaClass.simpleName)
                } else {
                    loaderFragment.dismiss()
                }
            })

            showErrorObserver.observe(this@CurrentTripFragment, Observer {
                showAlertWithAction(getString(it)) {
                    requireActivity().finish()
                    return@showAlertWithAction true
                }
            })
            showExceptionObserver.observe(this@CurrentTripFragment, Observer {
                showAlertWithAction(it) {
                    requireActivity().finish()
                    return@showAlertWithAction true
                }
            })
        }

        with(tripViewModel.updateTripStatusLiveData) {
            customObserver.observe(this@CurrentTripFragment, Observer {
                onSuccessUpdateTrip(it)
            })

            loadingObserver.observe(this@CurrentTripFragment, Observer {
                if (it == View.VISIBLE) {
                    loaderFragment.show(childFragmentManager, javaClass.simpleName)
                } else {
                    loaderFragment.dismiss()
                }
            })

            showErrorObserver.observe(this@CurrentTripFragment, Observer {
                showAlertWithResource(it)
            })
            showExceptionObserver.observe(this@CurrentTripFragment, Observer {
                showAlert(it)
            })
        }

    }

    private fun loadArguments() {
        this.arguments?.let {
            this.tripEntity = it.getSerializable(TRIP_ENTITY) as TripEntity?
        }
        val userString: String? = preferencesManager?.getSharedPreference(Constants.CURRENT_PASSENGER)
        userString?.let {
            userEntity = Gson().fromJson(userString, UserEntity::class.java)
        }
        showTripDataByStatus(tripEntity?.status ?: "")
    }

    @SuppressLint("SetTextI18n")
    private fun configureButtons() {

        btnTakeTrip.clickEvent().observe(this, Observer {
            tripViewModel.takeTrip(assignDriverToTripRequest)
        })

        btnStartTrip.clickEvent().observe(this, Observer {
            startTrip()
        })

        btnCancelTrip.clickEvent().observe(this, Observer {
            cancelTrip()
        })

        btnEndTrip.clickEvent().observe(this, Observer {
            finishTrip()
        })

        this.btnNavigate.clickEvent().observe(this, Observer {
            route?.let {
                val options = NavigationLauncherOptions.builder()
                    .directionsRoute(it)
                    .shouldSimulateRoute(false)
                    .build()
                NavigationLauncher.startNavigation(requireActivity(), options)
            }
        })

        imgArrowIcon.clickEvent().observe(this, Observer {
            showOrHideTripInfoContainer()
        })
    }

    private fun startTrip() {
        status = TripStatus.STARTED
        tripViewModel.updateTripStatus(updateTripRequest)
    }

    private fun cancelTrip() {
        val message = if (tripEntity?.status?.toLowerCase() != TripStatus.FIRST.desc) {
            "¿Estás seguro que deseas cancelar el viaje?"
        } else {
            "¿Estás seguro que deseas rechazar el viaje?"
        }
        createDialog(
            requireContext(),
            message,
            okBtn = "Sí",
            noBtn = "No",
            positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()

                cancelTripProcess()

            },
            negativeListener = DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()
            }
        )

    }

    private fun cancelTripProcess() {
        if (tripEntity?.status?.toLowerCase() == TripStatus.FIRST.desc) {
            disableTrip()
            goToHome()
        } else {
            status = TripStatus.CANCELLED
            tripEntity?._canceladoPor = "conductor"
            tripViewModel.updateTripStatus(updateTripRequest)
        }
    }

    private fun goToHome() {
        val intent = intentFor<HomeActivity>()
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        requireActivity().finish()
    }

    private fun finishTrip() {
        status = TripStatus.FINISHED
        tripViewModel.updateTripStatus(updateTripRequest)
    }

    private fun disableTrip() {
        this.preferencesManager?.clearPreferences(Constants.CURRENT_TRIP)
        this.preferencesManager?.clearPreferences(Constants.CURRENT_PASSENGER)
    }

    private fun saveTrip(tripEntity: TripEntity?) {
        tripEntity?.let {
            this.preferencesManager?.setSharedPreference(
                Constants.CURRENT_TRIP,
                Gson().toJson(it, TripEntity::class.java)
            )
        }
    }

    private fun saveUser(user: UserEntity) {
        this.preferencesManager?.setSharedPreference(
            Constants.CURRENT_PASSENGER,
            Gson().toJson(user, UserEntity::class.java)
        )
    }

    private fun onTakeTripSuccess(data: TripAndUserEntity) {
        saveTrip(data.trip)
        saveUser(data.user)
        userEntity = data.user
        tripEntity = data.trip
        //atvTrip.text = "Recoge a ${data.user.name} en ${data.trip.domicilioOrigen}"
        showTripDataByStatus(data.trip.status)
    }

    private fun onSuccessUpdateTrip(data: TripEntity) {
        tripEntity = data
        saveTrip(tripEntity)
        showTripDataByStatus(data.status)
    }

    private var tryAgainRoute = false
    private fun showTripDataByStatus(status: String) {
        imgArrowIcon.visible()
        when(status.toLowerCase()) {
            TripStatus.FIRST.desc -> {
                imgArrowIcon.gone()
                btnTakeTrip.visible()
                btnEndTrip.gone()
                btnStartTrip.gone()
                btnNavigate.gone()
                btnCancelTrip.text = getString(R.string.reject)
                tripEntity?.let {
                    if (::originalLocation.isInitialized) {
                        this.generateRoute(
                            LatLng(originalLocation.latitude, originalLocation.longitude),
                            LatLng(it.latitudOrigen, it.longitudOrigen)
                        )
                    } else {
                        tryAgainRoute = true
                    }
                }

                tvOriginPoint.text = this.tripEntity?.domicilioOrigen
            }
            TripStatus.WAITING.desc -> {
                btnTakeTrip.gone()
                btnEndTrip.gone()
                btnCancelTrip.text = getString(R.string.cancel)
                btnStartTrip.visible()
                btnNavigate.visible()

                tvDistance.text = String.format("Recoge a %s en:", userEntity?.name)
                tvOriginPoint.text = tripEntity?.domicilioOrigen

                tripEntity?.let {
                    if (::originalLocation.isInitialized) {
                        this.generateRoute(
                            LatLng(originalLocation.latitude, originalLocation.longitude),
                            LatLng(it.latitudOrigen, it.longitudOrigen)
                        )
                    } else {
                        tryAgainRoute = true
                    }
                }
            }
            TripStatus.STARTED.desc,
            TripStatus.IN_PROCESS.desc -> {
                btnTakeTrip.gone()
                btnStartTrip.gone()
                btnEndTrip.visible()
                btnNavigate.visible()
                btnCancelTrip.text = getString(R.string.cancel)
                tvDistance.text = String.format("Lleva a %s a:", userEntity?.name)
                tvOriginPoint.text = tripEntity?.domicilioDestino

                tripEntity?.let {
                    if (::originalLocation.isInitialized) {
                        this.generateRoute(
                            LatLng(originalLocation.latitude, originalLocation.longitude),
                            LatLng(it.latitudDestino, it.longitudDestino)
                        )
                    } else {
                        tryAgainRoute = true
                    }
                }
            }
            TripStatus.FINISHED.desc,
            TripStatus.CANCELLED.desc -> {
                disableTrip()
                goToHome()
            }
            else -> {
                //:)
            }
        }
    }

    private fun generateRoute(origin: LatLng, destiny: LatLng) {
        this.destinationMarker?.let { marker ->
            map.removeMarker(marker)
        }
        destinationPoint = Point.fromLngLat(
            destiny.longitude,
            destiny.latitude
        )
        destinationMarker = map.addMarker(MarkerOptions().position(destiny))
        originalPosition = Point.fromLngLat(
            origin.longitude,
            origin.latitude
        )



        this.getRoute(originalPosition, destinationPoint)
    }

    companion object {
        private const val TRIP_ENTITY = "trip.entity"

        fun newInstance(tripEntity: TripEntity?) =
            CurrentTripFragment()
                .apply {
                    arguments = bundleOf(
                        TRIP_ENTITY to tripEntity
                    )
                }
    }

    private fun enableLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!this.permissionManager.requestSinglePermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.initializeLocationEngine()
            }
        } else {
            if (permissionManager.permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.initializeLocationEngine()
            }
        }
    }


    override fun onPermissionDenied(permission: String) {

    }

    override fun onPermissionGranted(permission: String) {
        //this.initializeLocationEngine()
        this.initializeLocationLayer()
    }

    override fun onResume() {
        super.onResume()
        this.mapView?.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        this.mapView?.onSaveInstanceState(outState)
    }

    override fun onPause() {
        super.onPause()
        this.mapView?.onPause()
    }

    @SuppressLint("MissingPermission")
    override fun onStart() {
        super.onStart()
        if (permissionManager.permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            //initializeLocationEngine()
            locationLayerPlugin?.onStart()
        }
        this.mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        this.mapView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        this.mapView?.onLowMemory()
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        map = mapboxMap
        enableLocation()
        loadArguments()
    }

    private var showing = true
    private fun showOrHideTripInfoContainer() {
        if (showing) {
            tripInfoContainer.animate()
                .translationY(tripInfoContainer.height.toFloat() * 0.95f)
                .alpha(1.0f)
                .start()
            showing = false
            imgArrowIcon.setImageDrawable(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_keyboard_arrow_up_24)
            )
        } else {
            tripInfoContainer.animate()
                .translationY(0f)
                .alpha(1.0f)
                .start()
            showing = true
            imgArrowIcon.setImageDrawable(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_keyboard_arrow_down_24)
            )
        }
    }

    inner class LocationChangeListener(
        private val fragment: CurrentTripFragment
    ) : LocationEngineCallback<LocationEngineResult> {

        private val fragmentWeakReference: WeakReference<CurrentTripFragment> by lazy {
            WeakReference(fragment)
        }

        override fun onSuccess(result: LocationEngineResult?) {
            fragmentWeakReference.get()?.let { fragment ->
                result?.lastLocation?.let { location ->
                    originalLocation = location
                    fragment.map.locationComponent.forceLocationUpdate(location)
                    if (tryAgainRoute) {
                        tryAgainRoute = false
                        showTripDataByStatus(tripEntity?.status ?: "")
                    }

                    //Solo actualizar cuando ya aceptó el viaje
                    if (tripEntity?.status?.toLowerCase() != TripStatus.FIRST.desc) {
                        setCameraPosition(location)
                    }
                }
            }
        }

        override fun onFailure(exception: Exception) {
            Timber.e(exception)
        }

    }

}