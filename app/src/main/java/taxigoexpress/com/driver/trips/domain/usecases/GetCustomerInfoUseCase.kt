package taxigoexpress.com.driver.trips.domain.usecases

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import taxigoexpress.com.driver.trips.domain.entities.request.GetCustomerInfoRequest
import taxigoexpress.com.driver.trips.domain.repository.TripRepository
import javax.inject.Inject

class GetCustomerInfoUseCase @Inject constructor(
    private val tripRepository: TripRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<UserEntity, GetCustomerInfoRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: GetCustomerInfoRequest): Observable<UserEntity> =
        tripRepository.getCustomerInfo(params)

}