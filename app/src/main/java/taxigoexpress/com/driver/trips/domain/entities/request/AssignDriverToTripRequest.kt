package taxigoexpress.com.driver.trips.domain.entities.request

import com.taxigoexpress.core.domain.entities.TripEntity

data class AssignDriverToTripRequest(
    val tripId: String,
    val driverId: String,
    val tripEntity: TripEntity
)