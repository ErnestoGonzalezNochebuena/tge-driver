package taxigoexpress.com.driver.trips.domain.entities.request

data class GetCustomerInfoRequest(
    val customerId: String
)