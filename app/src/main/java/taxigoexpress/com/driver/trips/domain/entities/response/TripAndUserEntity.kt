package taxigoexpress.com.driver.trips.domain.entities.response

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity

data class TripAndUserEntity(
    val trip: TripEntity,
    val user: UserEntity
)