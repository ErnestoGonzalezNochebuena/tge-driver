package taxigoexpress.com.driver.trips.presentation.activities

import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.presentation.activities.BaseActivity
import taxigoexpress.com.driver.trips.presentation.fragments.CurrentTripFragment
import com.taxigoexpress.core.presentation.utils.Constants
import taxigoexpress.com.driver.R


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class TripActivity : BaseActivity() {
    private var tripEntity: TripEntity? = null

    override fun getLayoutResId(): Int = R.layout.trip_activity_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.loadArguments(intent)
        this.launchView()
    }


    private fun launchView() {
        val currentTrip = Gson().fromJson(
            this.preferencesManager.getSharedPreference(
                Constants.CURRENT_TRIP,
                ""
            ), TripEntity::class.java
        )
        currentTrip?.let {
            this.tripEntity = it
        }
        this.replaceFragment(
            CurrentTripFragment.newInstance(this.tripEntity),
            R.id.tripContainer,
            false
        )
    }


    private fun loadArguments(intent: Intent?) {
        intent?.extras?.let {
            this.tripEntity = it["trip"] as TripEntity
        }
    }

    override fun onBackPressed() {

    }

}