package taxigoexpress.com.driver.trips.presentation.viewmodels.impl

import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.presentation.observers.GenericObserver
import com.taxigoexpress.core.presentation.view.BaseLiveData
import taxigoexpress.com.driver.trips.domain.entities.request.AssignDriverToTripRequest
import taxigoexpress.com.driver.trips.domain.entities.request.UpdateTripStatusRequest
import taxigoexpress.com.driver.trips.domain.entities.response.TripAndUserEntity
import taxigoexpress.com.driver.trips.domain.usecases.*
import taxigoexpress.com.driver.trips.presentation.viewmodels.abs.TripViewModel
import javax.inject.Inject

class TripViewModelImpl @Inject constructor(
    private val updateTripStatusUseCase: UpdateTripStatusUseCase,
    private val takeTripUseCase: TakeTripUseCase
) : TripViewModel() {

    override val takeTripLiveData: BaseLiveData<TripAndUserEntity> by lazy {
        buildLiveData<TripAndUserEntity>()
    }

    override val updateTripStatusLiveData: BaseLiveData<TripEntity> by lazy {
        buildLiveData<TripEntity>()
    }

    override fun takeTrip(request: AssignDriverToTripRequest) {
        takeTripUseCase.execute(GenericObserver(takeTripLiveData), request)
    }

    override fun updateTripStatus(request: UpdateTripStatusRequest) {
        updateTripStatusUseCase.execute(GenericObserver(updateTripStatusLiveData), request)
    }

    override fun onCleared() {
        updateTripStatusUseCase.dispose()
        takeTripUseCase.dispose()
        super.onCleared()
    }

}