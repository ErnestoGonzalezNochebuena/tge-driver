package taxigoexpress.com.driver.profile.data.repositoryImplementations

import taxigoexpress.com.driver.profile.data.datasourceImplementations.ProfileDataSourceImpl
import taxigoexpress.com.driver.profile.domain.repositoryAbstractions.ProfileRepository
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import javax.inject.Inject

class ProfileRepositoryImpl
@Inject constructor(private val profileDataSourceImpl: ProfileDataSourceImpl) : ProfileRepository {

    override fun getProfileInformation(profileId: String): Observable<UserEntity> =
        this.profileDataSourceImpl.getProfileInformation(profileId)
}