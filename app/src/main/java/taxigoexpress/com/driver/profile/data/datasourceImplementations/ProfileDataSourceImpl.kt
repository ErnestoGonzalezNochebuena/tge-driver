package taxigoexpress.com.driver.profile.data.datasourceImplementations

import taxigoexpress.com.driver.profile.data.services.ProfileServices
import taxigoexpress.com.driver.profile.domain.datasourceAbstractions.ProfileDataSource
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import javax.inject.Inject

class ProfileDataSourceImpl
@Inject constructor(private val service: ProfileServices) : ProfileDataSource {

    override fun getProfileInformation(profileId: String): Observable<UserEntity> =
        this.service.getProfile(profileId).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }
}
