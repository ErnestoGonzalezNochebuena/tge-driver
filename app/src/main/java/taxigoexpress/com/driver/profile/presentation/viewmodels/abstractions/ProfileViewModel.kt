package taxigoexpress.com.driver.profile.presentation.viewmodels.abstractions

import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData

interface ProfileViewModel<T> : PresenterLiveData<T>{
    fun getProfileInformation(profileId:String)
    fun observeProfileInformation(): BaseLiveData<T>
}
