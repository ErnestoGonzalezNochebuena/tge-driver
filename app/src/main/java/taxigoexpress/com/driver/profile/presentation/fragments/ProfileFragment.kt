package taxigoexpress.com.driver.profile.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import taxigoexpress.com.driver.HomeActivity

import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.profile.presentation.components.DaggerProfileComponent
import taxigoexpress.com.driver.profile.presentation.components.ProfileComponent
import taxigoexpress.com.driver.profile.presentation.modules.ProfileModule
import taxigoexpress.com.driver.profile.presentation.viewmodels.abstractions.ProfileViewModel
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : BaseFragment() {

    @Inject
    lateinit var profileViewModel: ProfileViewModel<UserEntity>

    private val profileComponent: ProfileComponent by lazy {
        DaggerProfileComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .profileModule(ProfileModule())
            .build()
    }

    override fun getLayout(): Int = R.layout.fragment_profile_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        profileComponent.inject(this)
        this.configureToolbar()
    }

    private fun configureToolbar() {
        val homeActivity = this.activity as HomeActivity
        homeActivity.showSectionToolbar()
        homeActivity.changeTitleToolbar("Perfil")
    }

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }


}
