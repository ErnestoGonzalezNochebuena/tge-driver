package taxigoexpress.com.driver.statistics.presentation.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.inflate
import com.taxigoexpress.core.presentation.utils.clickEventObservable
import kotlinx.android.synthetic.main.item_indicator_stadistics.view.*
import org.jetbrains.anko.textColor
import taxigoexpress.com.driver.R

class IndicatorsAdapter(private val context: Context) :
    RecyclerView.Adapter<IndicatorsAdapter.Viewholder>() {

    private var intAdapterPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder =
        Viewholder(
            parent.inflate(R.layout.item_indicator_stadistics)
        )

    override fun getItemCount(): Int = 6

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        holder.updateItem(position, context,this.intAdapterPosition)
        holder.itemView.clickEventObservable().subscribe {
            this.setAdapterPosition(position)
        }
    }

    fun setAdapterPosition(position: Int) {
        this.intAdapterPosition = position
        this.notifyDataSetChanged()
    }


    class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateItem(position: Int, context: Context, intAdapterPosition:Int) {
            if (position == intAdapterPosition) {
                itemView.mcvMain.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
                itemView.tvTitleIndicator.textColor = ContextCompat.getColor(context, R.color.white)
                itemView.tvContentIndicator.textColor =
                    ContextCompat.getColor(context, R.color.white)
            } else {
                itemView.mcvMain.setCardBackgroundColor(ContextCompat.getColor(context, R.color.navigation_view_background_section))
                itemView.tvTitleIndicator.textColor =
                    ContextCompat.getColor(context, R.color.md_black_1000)
                itemView.tvContentIndicator.textColor =
                    ContextCompat.getColor(context, R.color.md_black_1000)
            }
        }

    }


}
