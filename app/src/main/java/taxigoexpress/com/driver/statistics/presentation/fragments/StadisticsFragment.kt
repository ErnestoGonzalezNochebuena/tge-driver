package taxigoexpress.com.driver.statistics.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager.widget.ViewPager
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_stadistics_layout.*
import kotlinx.android.synthetic.main.stadistics_indicators_layout.*
import taxigoexpress.com.driver.HomeActivity

import taxigoexpress.com.driver.R
import taxigoexpress.com.driver.statistics.presentation.adapters.IndicatorsAdapter
import taxigoexpress.com.driver.statistics.presentation.adapters.StadisticsViewPagerAdapter

/**
 * A simple [Fragment] subclass.
 */
class StadisticsFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_stadistics_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.loadView()
    }

    private fun configureToolbar() {
        val homeActivity = this.activity as HomeActivity
        homeActivity.showSectionToolbar()
        homeActivity.changeTitleToolbar("Estadísticas")
    }

    private fun loadView() {

        val periodAdapter =
            StadisticsViewPagerAdapter(
                this.childFragmentManager
            )
        this.vpStadistics.adapter = periodAdapter
        this.tlStadistics.setupWithViewPager(this.vpStadistics)
        this.vpStadistics.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {

            }
        })

        this.context?.let {
            val indicatorAdapter =
                IndicatorsAdapter(
                    it
                )
            this.rvStadistics.adapter = indicatorAdapter
            this.rvStadistics.layoutManager = GridLayoutManager(it, 6)
        }


    }

    companion object {
        @JvmStatic
        fun newInstance(): StadisticsFragment {
            return StadisticsFragment()
        }
    }


}
