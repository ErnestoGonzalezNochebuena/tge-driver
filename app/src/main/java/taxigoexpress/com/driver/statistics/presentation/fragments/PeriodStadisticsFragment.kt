package taxigoexpress.com.driver.statistics.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.taxigoexpress.core.presentation.fragments.BaseFragment

import taxigoexpress.com.driver.R

/**
 * A simple [Fragment] subclass.
 */
class PeriodStadisticsFragment : BaseFragment() {

    override fun getLayout(): Int  = R.layout.fragment_period_stadistics_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }

    companion object{
        fun newInstance(): PeriodStadisticsFragment {
            return PeriodStadisticsFragment()
        }
    }


}
