package com.taxigoexpress.login.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface LoginServices {

    @POST(BuildConfig.LOGIN)
    fun login(
        @Body loginRequest: LoginRequest
    ):Observable<Response<Unit>>

    @GET(BuildConfig.VALIDATE_DRIVER)
    fun validateDriver(
        @Path("email") email: String
    ): Observable<Response<LoginResponse>>

    @GET(BuildConfig.GET_PROFILE)
    fun getProfile(
        @Path("id") profileId: String
    ): Observable<Response<UserEntity>>
}