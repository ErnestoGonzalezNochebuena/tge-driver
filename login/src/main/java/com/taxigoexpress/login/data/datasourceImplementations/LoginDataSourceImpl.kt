package com.taxigoexpress.login.data.datasourceImplementations

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.login.data.services.LoginServices
import com.taxigoexpress.login.domain.datasourceAbstractions.LoginDataSource
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import javax.inject.Inject

class LoginDataSourceImpl
@Inject constructor(private val service: LoginServices) : LoginDataSource {

    override fun login(params: LoginRequest): Observable<Unit> =
        this.service.login(params)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(Unit)
                } else {
                    Observable.error(Throwable(it.message()))
                }
            }

    override fun validateDriver(email: String): Observable<LoginResponse> =
        this.service.validateDriver(email)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(Throwable(it.message()))
                }
            }

    override fun getProfile(params: String): Observable<UserEntity> =
        this.service.getProfile(params)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(Throwable(it.message()))
                }
            }


}
