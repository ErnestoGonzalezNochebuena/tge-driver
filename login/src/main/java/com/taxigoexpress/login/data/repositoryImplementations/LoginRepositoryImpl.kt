package com.taxigoexpress.login.data.repositoryImplementations

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.login.data.datasourceImplementations.LoginDataSourceImpl
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.login.domain.repositoryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginRepositoryImpl
@Inject constructor(private val signinDatasourceImpl: LoginDataSourceImpl): LoginRepository {

    override fun login(params: LoginRequest): Observable<Unit> =
        this.signinDatasourceImpl.login(params)

    override fun validateDriver(params: String): Observable<LoginResponse> =
        this.signinDatasourceImpl.validateDriver(params)


    override fun getProfile(params: String): Observable<UserEntity> =
        this.signinDatasourceImpl.getProfile(params)


}