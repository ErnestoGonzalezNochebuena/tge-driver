package com.taxigoexpress.login.presentation.liveData

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData

data class LoginLiveData(
    val loginObserver: BaseLiveData<UserEntity>,
    val emailErrorObserver:MutableLiveData<Int> = MutableLiveData(),
    val passwordErrorObserver: MutableLiveData<Int> = MutableLiveData()
)
