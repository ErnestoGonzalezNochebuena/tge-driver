package com.taxigoexpress.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.R
import kotlinx.android.synthetic.main.fragment_password_recovery_layout.*

/**
 * A simple [Fragment] subclass.
 */
class PasswordRecoveryFragment : BaseFragment() {

    override fun getLayout(): Int = R.layout.fragment_password_recovery_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadView()
    }

    private fun loadView() {
        this.btnSendEmail.clickEvent().observe(this, Observer {
            this.activity?.supportFragmentManager?.popBackStack()
        })
    }

    companion object {
        fun newInstance(): PasswordRecoveryFragment {
            return PasswordRecoveryFragment()
        }
    }


}
