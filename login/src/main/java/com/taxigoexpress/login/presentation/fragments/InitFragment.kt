package com.taxigoexpress.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.R
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.presentation.activities.LoginActivity
import com.taxigoexpress.login.presentation.components.DaggerLoginComponent
import com.taxigoexpress.login.presentation.components.LoginComponent
import com.taxigoexpress.login.presentation.modules.LoginModule
import com.taxigoexpress.login.presentation.viewmodels.abstractions.LoginViewModel
import com.taxigoexpress.register.presentation.activities.RegisterActivity
import kotlinx.android.synthetic.main.fragment_init_layout.*
import org.jetbrains.anko.support.v4.intentFor
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class InitFragment : BaseFragment() {

    @Inject
    lateinit var loginViewModel: LoginViewModel<UserEntity>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(com.taxigoexpress.register.R.string.loading)) }

    private val loginComponent: LoginComponent by lazy {
        DaggerLoginComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .loginModule(LoginModule())
            .build()
    }

    override fun getLayout(): Int = R.layout.fragment_init_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        loginComponent.inject(this)
        this.configureToolbar()
        this.loadView()
        this.observeResults()
        this.keyStoreManager?.retrieveDataWithSafeMode(Constants.DRIVER_INFO)?.let {
            if (it.isNotEmpty()) {
                this.activity?.finish()
            }
//            this.replaceFragment(
//                DocumentsFragment.newInstance(Document.AddressDocument),
//                R.id.login_container,
//                true
//            )
        }
    }

    private fun startSession() {
        this.loginViewModel.login(
            this.keyStoreManager?.retrieveDataWithSafeMode(Constants.DRIVER_INFO)?.let { user ->
                this.keyStoreManager?.retrieveDataWithSafeMode(Constants.PASSWORD)
                    ?.let { password ->
                        LoginRequest(
                            user,
                            password
                        )
                    }
            }!!
        )
    }

    private fun configureToolbar() {
        (this.activity as LoginActivity).hideToolbar()
    }

    private fun loadView() {
        this.btnGotLogin.clickEvent().observe(this, Observer {
            this.replaceFragment(
                LoginFragment.newInstance(),
                R.id.login_container,
                true
            )
//          this.activity?.intentFor<LoginActivity>()
        })

        this.btnGoRegister.clickEvent().observe(this, Observer {
            this.activity?.startActivity(intentFor<RegisterActivity>())
            this.activity?.finish()
        })
    }

    private fun observeResults() {

        with(this.loginViewModel.observeLogin()) {
            this.loginObserver.customObserver.observe(this@InitFragment, Observer {
                this@InitFragment.activity?.finish()
            })

            this.loginObserver.loadingObserver.observe(this@InitFragment, Observer {
                if (it == View.VISIBLE) {
                    this@InitFragment.loaderDialog.show(
                        childFragmentManager,
                        LoginFragment::class.java.name
                    )
                } else {
                    this@InitFragment.loaderDialog.dismiss()
                }
            })


            this.loginObserver.showErrorObserver.observe(this@InitFragment, Observer {
                this@InitFragment.showAlertWithResource(it ?: 0)
            })

            this.loginObserver.showExceptionObserver.observe(
                this@InitFragment,
                Observer {
                    this@InitFragment.showAlert("Error al inicio de sesión")
                })

            this.emailErrorObserver.observe(this@InitFragment, Observer {
                this@InitFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@InitFragment, Observer {
                this@InitFragment.showAlertWithResource(it ?: 0)
            })
        }
    }

    companion object {
        fun newInstance(): InitFragment {
            return InitFragment()
        }
    }


}
