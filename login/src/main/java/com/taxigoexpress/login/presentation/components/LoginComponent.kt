package com.taxigoexpress.login.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.login.presentation.fragments.InitFragment
import com.taxigoexpress.login.presentation.fragments.LoginFragment
import com.taxigoexpress.login.presentation.modules.LoginModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [LoginModule::class]
)
interface LoginComponent {
    fun inject(loginFragment: LoginFragment)
    fun inject(initFragment: InitFragment)
}