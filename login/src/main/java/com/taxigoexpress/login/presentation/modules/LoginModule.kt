package com.taxigoexpress.login.presentation.modules

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.login.data.repositoryImplementations.LoginRepositoryImpl
import com.taxigoexpress.login.data.services.LoginServices
import com.taxigoexpress.login.domain.repositoryAbstractions.LoginRepository
import com.taxigoexpress.login.presentation.viewmodels.abstractions.LoginViewModel
import com.taxigoexpress.login.presentation.viewmodels.implementations.LoginViewModelImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class LoginModule {

    @Provides
    @FragmentScope
    fun providesLoginServices(@Named("main_retrofit") retrofit: Retrofit): LoginServices =
        retrofit.create(LoginServices::class.java)

    @Provides
    @FragmentScope
    fun providesLogiRepository(loginRepositoryImpl: LoginRepositoryImpl): LoginRepository =
        loginRepositoryImpl

    @Provides
    @FragmentScope
    fun provideLoginViewModel(loginViewModelImpl: LoginViewModelImpl): LoginViewModel<UserEntity> =
        loginViewModelImpl
}