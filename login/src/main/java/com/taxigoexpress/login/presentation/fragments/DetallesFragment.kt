package com.taxigoexpress.login.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.login.R
import com.taxigoexpress.login.presentation.activities.LoginActivity


/**
 * A simple [Fragment] subclass.
 */
class DetallesFragment : BaseFragment() {


    override fun getLayout(): Int = R.layout.fragment_detalles_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureToolbar()
        this.loadView()
    }

    private fun configureToolbar() {
        (this.activity as LoginActivity).hideToolbar()
    }

    private fun loadView() {

    }

    companion object {
        fun newInstance(): DetallesFragment {
            return DetallesFragment()
        }
    }


}
