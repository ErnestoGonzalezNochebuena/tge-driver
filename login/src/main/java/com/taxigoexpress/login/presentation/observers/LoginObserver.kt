package com.taxigoexpress.login.presentation.observers

import android.view.View
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.login.R
import com.taxigoexpress.login.domain.exceptions.LoginException
import com.taxigoexpress.login.presentation.liveData.LoginLiveData
import java.io.IOException

class LoginObserver(private val mLoginLiveData: LoginLiveData) :
    DefaultObserverLiveData<UserEntity>(mLoginLiveData.loginObserver) {

    override fun onStart() {
        this.mLoginLiveData.loginObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mLoginLiveData.loginObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: UserEntity) {
        this.mLoginLiveData.loginObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mLoginLiveData.loginObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mLoginLiveData.loginObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is LoginException -> useCase(error)
            else -> this.mLoginLiveData.loginObserver.showExceptionObserver.value = error.localizedMessage
        }
    }

    private fun useCase(error: LoginException) {
        when (error.validationType) {
            LoginException.Type.PASSWORD_EMPTY_ERROR -> this.mLoginLiveData.passwordErrorObserver.value =
                R.string.sign_in_empty_password_error
            LoginException.Type.EMAIL_WRONG_FORMAT-> this.mLoginLiveData.emailErrorObserver.value =
                R.string.sign_in_wrong_email_error
        }
    }

}
