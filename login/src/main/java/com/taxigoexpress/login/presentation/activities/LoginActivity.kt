package com.taxigoexpress.login.presentation.activities

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.login.R
import com.taxigoexpress.login.presentation.fragments.InitFragment
import com.taxigoexpress.login.presentation.fragments.LoginFragment

import kotlinx.android.synthetic.main.activity_login_layout.*
import kotlinx.android.synthetic.main.content_login_toolbar.*

class LoginActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_login_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.loadView()
        this.configureToolbar()
    }

    private fun loadView() {
        this.replaceFragment(InitFragment.newInstance(), R.id.login_container, false)
    }

    private fun configureToolbar() {
        this.ivToolbarBack.clickEvent().observe(this, Observer {
                if (this.supportFragmentManager.backStackEntryCount > 0) {
                    this.supportFragmentManager.popBackStack()
                } else {
                    this.finish()
                }

        })
    }

    override fun onBackPressed() {
        if (keyStoreManager.retrieveDataWithSafeMode(Constants.DRIVER_INFO).isEmpty()) {
            this.finish()
        } else {
            super.onBackPressed()
        }
    }


    fun hideToolbar() {
        this.login_appbar.gone()
    }

    fun showToolbar() {
        this.login_appbar.visible()
    }


}
