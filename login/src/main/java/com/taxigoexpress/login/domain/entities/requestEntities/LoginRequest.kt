package com.taxigoexpress.login.domain.entities.requestEntities

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @field:SerializedName("correo")
    val _email: String,

    @field:SerializedName("password")
    val _password: String
)
