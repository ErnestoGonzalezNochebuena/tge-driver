package com.taxigoexpress.login.domain.exceptions

class LoginException
    @JvmOverloads constructor(
        val validationType: Type, message: String? = "SignIn ${validationType.name}", cause: Throwable? = null
    ) :
            Throwable(message, cause) {

    enum class Type {
        EMAIL_WRONG_FORMAT,
        PASSWORD_EMPTY_ERROR,
        NOT_PASSENGER
    }
}