package com.taxigoexpress.login.domain.repositoryAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable


interface LoginRepository {
    fun login(params: LoginRequest): Observable<Unit>
    fun validateDriver(params: String): Observable<LoginResponse>
    fun getProfile(params: String): Observable<UserEntity>

}
