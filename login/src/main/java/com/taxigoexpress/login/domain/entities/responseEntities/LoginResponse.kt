package com.taxigoexpress.login.domain.entities.responseEntities

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("global")
    val _globalUser: Int? = null,

    @SerializedName("user")
    val _driverId: Int? = null,

    @SerializedName("type")
    var _userType: Int = 0
) {

}
