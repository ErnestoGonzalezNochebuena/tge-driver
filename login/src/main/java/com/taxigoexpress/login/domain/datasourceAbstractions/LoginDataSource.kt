package com.taxigoexpress.login.domain.datasourceAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.login.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.login.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable

interface LoginDataSource {
    fun login(params: LoginRequest): Observable<Unit>
    fun validateDriver(email:String):Observable<LoginResponse>
    fun getProfile(params: String): Observable<UserEntity>

}
