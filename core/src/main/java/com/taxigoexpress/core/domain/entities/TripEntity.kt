package com.taxigoexpress.core.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


data class TripEntity(

    @Expose
    @SerializedName("id")
    val _id: Int? = null,

    @Expose
    @SerializedName("id_pas")
    val _id_pas: Int? = null,

    @Expose
    @SerializedName("domicilioOrigen")
    val _domicilioOrigen: String? = null,
    @Expose
    @SerializedName("domicilioDestino")
    val _domicilioDestino: String? = null,
    @Expose
    @SerializedName("latitudOrigen")
    val _latitudOrigen: Double? = null,
    @Expose
    @SerializedName("latitudDestino")
    val _latitudDestino: Double? = null,
    @Expose
    @SerializedName("longitudOrigen")
    val _longitudOrigen: Double? = null,
    @Expose
    @SerializedName("longitudDestino")
    val _longitudDestino: Double? = null,
    @Expose
    @SerializedName("latitudActual")
    val _latitudActual: Double? = null,
    @Expose
    @SerializedName("longitudActual")
    val _longitudActual: Double? = null,
    @Expose
    @SerializedName("fechaCreacion")
    val _fechaCreacion: String? = null,
    @Expose
    @SerializedName("fechaFinalizacion")
    val _fechaFinalizacion: String? = null,
    @Expose
    @SerializedName("costoTotal")
    val _costoTotal: Double? = null,
    @Expose
    @SerializedName("status")
    val _status: String? = null,
    @Expose
    @SerializedName("fechaEsperando")
    val _fechaEsperando: String? = null,
    @Expose
    @SerializedName("fechaEnCurso")
    val _fechaEnCurso: String? = null,
    @Expose
    @SerializedName("canceladoPor")
    var _canceladoPor: String? = null,
    @Expose
    @SerializedName("tipoPago")
    val _tipoPago: String? = null,
    @Expose
    @SerializedName("id_promo")
    val _idPromo: Int? = null
) : Serializable {

    val id: Int
        get() = _id ?: 0

    val id_pas: Int
        get() = _id_pas ?: 0

    val domicilioOrigen: String
        get() = _domicilioOrigen ?: ""

    val domicilioDestino: String
        get() = _domicilioDestino ?: ""

    val latitudOrigen: Double
        get() = _latitudOrigen ?: 0.0

    val latitudDestino: Double
        get() = _latitudDestino ?: 0.0

    val longitudOrigen: Double
        get() = _longitudOrigen ?: 0.0

    val longitudDestino: Double
        get() = _longitudDestino ?: 0.0

    val latitudActual: Double
        get() = _latitudActual ?: 0.0

    val longitudActual: Double
        get() = _longitudActual ?: 0.0

    val fechaCreacion: String
        get() = _fechaCreacion ?: ""

    val fechaFinalizacion: String
        get() = _fechaFinalizacion ?: ""

    val fechaEsperando: String
        get() = _fechaEsperando ?: ""

    val fechaEnCurso: String
        get() = _fechaEnCurso ?: ""

    val tipoPago: String
        get() = _tipoPago ?: ""

    val idPromo: Int
        get() = _idPromo ?: 0

    val status: String get() = _status ?: ""

    val statusUpperCase get() = (_status ?: "").toUpperCase(Locale.getDefault())
}

