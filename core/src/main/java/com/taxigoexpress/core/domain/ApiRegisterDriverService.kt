package com.taxigoexpress.core.domain

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiRegisterDriverService {

    @PUT(BuildConfig.REGISTER_DRIVER)
    fun updateRegisterDriver(
        @Path("id") idDriver: String,
        @Body userEntity: UserEntity
    ): Call<TripEntity>
}