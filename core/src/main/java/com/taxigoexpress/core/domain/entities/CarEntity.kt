package com.taxigoexpress.core.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.taxigoexpress.core.domain.entities.BaseResponse

data class CarEntity(

    val idCond: String,

    @Expose
    @SerializedName("numAsientos")
    val _numPlaces: Int? = null,

    @Expose
    @SerializedName("color")
    val _color: String? = null,

    @Expose
    @SerializedName("modelo")
    val _model: String? = null,

    @Expose
    @SerializedName("placas")
    val _registerCar: String? = null,

    @Expose
    @SerializedName("marca")
    val _brandCar: String? = null

) : BaseResponse() {

}
