package com.taxigoexpress.core.domain

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.TripEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import retrofit2.Call
import retrofit2.http.*

import rx.Observable


interface ApiService {

    @PUT(BuildConfig.ADD_DRIVER_IN_TRIP)
    fun assignDriverToTrip(
        @Path("trip_id") tripId: String,
        @Path("id_cond") driverId: String,
        @Body tripEntity: TripEntity
    ): Call<TripEntity?>

    @PUT(BuildConfig.UPDATE_TRIP_ON_WAY)
    fun updateTrip(
        @Path("id") tripId: String,
        @Path("status") status: String,
        @Body tripEntity: TripEntity
    ): Call<TripEntity?>

    @GET(BuildConfig.GET_TRIP_CLIENT)
    fun getClientTrip(
        @Path("id") clientId: String
    ): Call<UserEntity?>

    @GET(BuildConfig.GET_TRIP_INFO)
    fun checkAvailability(
        @Path("trip_id") tripId: String
    ): Call<TripEntity?>

}