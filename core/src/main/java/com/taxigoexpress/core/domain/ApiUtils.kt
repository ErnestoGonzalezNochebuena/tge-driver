package com.taxigoexpress.core.domain

object ApiUtils {
    private const val BASE_URL = "http://50.21.189.124:8080"
    val apiService: ApiService?
        get() = RetrofitClient.getClient(BASE_URL)?.create(ApiService::class.java)

    val apiRegisterDriverService: ApiRegisterDriverService?
        get() = RetrofitClient.getClient(BASE_URL)?.create(ApiRegisterDriverService::class.java)
}