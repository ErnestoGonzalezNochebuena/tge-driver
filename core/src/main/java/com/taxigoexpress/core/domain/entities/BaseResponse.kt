package com.taxigoexpress.core.domain.entities

import com.google.gson.annotations.SerializedName

open class BaseResponse(

    @SerializedName(value = "status", alternate = ["ResponseCode", "BaseCode"])
    private var _status: Int? = null,

    @SerializedName("message")
    private var _message: String? = null

)
