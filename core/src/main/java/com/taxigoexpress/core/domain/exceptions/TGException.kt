package com.taxigoexpress.core.domain.exceptions

open class TGException : Exception {

    private var errorCode = "00"

    /**
     * Generates TGE exception
     * @param detailMessage message
     */
    constructor(detailMessage: String, errorCode: String = "") : super(detailMessage) {
        this.errorCode = errorCode
    }

    /**
     * Generates TGE exception
     * @param detailMessage message
     * @param throwable original throwable
     */
    constructor(detailMessage: String, throwable: Throwable, errorCode: String = "00") :
            super(detailMessage, throwable) {
        this.errorCode = errorCode
    }

    fun getErrorCode() = errorCode
}