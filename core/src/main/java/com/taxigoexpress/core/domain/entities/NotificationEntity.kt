package com.taxigoexpress.core.domain.entities

import com.google.gson.annotations.SerializedName

data class NotificationEntity(
    @SerializedName("title")
    var title: String = "",
    @SerializedName("message")
    var message: String = "",
    @SerializedName("body")
    var body: String = "") {

    override fun toString(): String {
        return "NotificationEntity(title='$title', body='$body')"
    }
}