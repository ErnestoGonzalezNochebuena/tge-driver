package com.taxigoexpress.core.presentation.utils

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.RectF
import android.net.Uri
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.Group
import com.google.android.material.textfield.TextInputLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.core.content.ContextCompat
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.invisible
import com.ia.mchaveza.kotlin_library.visible
import com.jakewharton.rxbinding.view.RxView
import com.jakewharton.rxbinding.widget.RxTextView
import com.taxigoexpress.core.R
import io.reactivex.Completable
import io.reactivex.Observer
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.subjects.ReplaySubject
import java.text.Normalizer
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

fun createDialog(
    context: Context,
    message: String = "Message",
    okBtn: String = context.getString(android.R.string.ok),
    noBtn: String = "",
    isCancelable: Boolean = true,
    positiveListener: DialogInterface.OnClickListener? = null,
    negativeListener: DialogInterface.OnClickListener? = null
): AlertDialog {
    val dialog = AlertDialog.Builder(context, R.style.BasicDialog)
        .setMessage(message)
        .setPositiveButton(okBtn, positiveListener)
        .setCancelable(isCancelable)
    if (noBtn.isNotEmpty()) {
        dialog.setNegativeButton(noBtn, negativeListener)
    }
    dialog.create()
    return dialog.show()
}

fun createDialogWithTitle(
    context: Context,
    title: String = "Title",
    message: String = "Message",
    okBtn: String = context.getString(android.R.string.ok),
    noBtn: String = "",
    isCancelable: Boolean = true,
    positiveListener: DialogInterface.OnClickListener? = null,
    negativeListener: DialogInterface.OnClickListener? = null
): AlertDialog {
    val dialog = AlertDialog.Builder(context, R.style.BasicDialog)
        .setMessage(message)
        .setTitle(title)
        .setPositiveButton(okBtn, positiveListener)
        .setCancelable(isCancelable)
    if (noBtn.isNotEmpty()) {
        dialog.setNegativeButton(noBtn, negativeListener)
    }
    dialog.create()
    return dialog.show()
}

fun View.fadeInAnimation(animationDuration: Long) {
    val fadeIn = AlphaAnimation(0f, 1f)
    fadeIn.apply {
        interpolator = AccelerateInterpolator()
        duration = animationDuration
    }
    this.animation = fadeIn
    this.visible()
}

fun View.fadeOutAnimation(animationDuration: Long) {
    val fadeOut = AlphaAnimation(1f, 0f)
    fadeOut.apply {
        interpolator = AccelerateInterpolator()
        duration = animationDuration
    }
    this.animation = fadeOut
    this.invisible()
}

fun View.clickEvent(): LiveData<Void> {
    val listener = MutableLiveData<Void>()
    RxView.clicks(this)
        .throttleFirst(1000, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            listener.value = it
        }
    return listener
}

fun Group.clickEvent(): LiveData<Void> {
    val listener = MutableLiveData<Void>()
    this.referencedIds.forEach { id ->
        val groupView = rootView.findViewById<View>(id)
        RxView.clicks(groupView)
            .throttleFirst(1000, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                listener.value = it
            }
    }
    return listener
}

fun View.clickEventObservable(): ReplaySubject<Void> {
    val replaySubject = ReplaySubject.create<Void>()
    RxView.clicks(this)
        .throttleFirst(1000, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            replaySubject.onNext(it)
        }
    return replaySubject
}

fun formatDateToDisplay(year: Int, month: String, dayOfMonth: Int): String {
    val mDay = if (dayOfMonth < 10) {
        "0".plus(dayOfMonth)
    } else {
        dayOfMonth.toString()
    }
    return mDay.plus(" / ").plus(month.toLowerCase()).plus(" / ").plus(year.toString())
}

fun String.substringBetween(firstDelimiter: String, secondDelimiter: String): String {
    val firstIndex = this.indexOf(firstDelimiter) + 1
    val secondIndex = this.indexOf(secondDelimiter)
    return if (secondIndex > firstIndex) {
        this.substring(firstIndex, secondIndex)
    } else {
        ""
    }
}

fun String.substringBetween(index: Int, secondDelimiter: String): String {
    val secondIndex = this.indexOf(secondDelimiter)
    return if (secondIndex > index) {
        this.substring(index, secondIndex)
    } else {
        ""
    }
}

fun String.parseAsHtml(): Spanned {
    return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun Editable.performFormatting(module: Int) {
    val space = ' '
    if ((this.length % module) == 0) {
        val currentCharacter = this[this.length - 1]
        if (space == currentCharacter) {
            this.delete(this.length - 1, this.length)
        }
        val char = this[this.length - 1]
        if (Character.isDigit(char) && TextUtils.split(
                this.toString(),
                space.toString()
            ).size <= 3
        ) {
            this.insert(this.length - 1, space.toString())
        }
    }
}

fun String.isValidEmail(): Boolean {
    val emailPattern =
        ("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
    val pattern = Pattern.compile(emailPattern)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}

fun String.isValidPassword(): Boolean {
    val passwordPattern =
        "(?=.*[a-z])" +     //At least one lower case character (a-z)
                "(?=.*\\d)" +   //At least one digit (0-9)
                ".*"
    val pattern = Pattern.compile(passwordPattern)
    val matcher = pattern.matcher(this)
    return matcher.matches() && this.length >= 8
}

fun String.isValidCardNumber(): Boolean {
    val multiplication = 2
    val chars = this.toCharArray()

    val digits = ArrayList<Int>()
    for (c in chars) {
        if (Character.isDigit(c)) {
            digits.add(c - '0')
        }
    }
    val length = digits.size
    var sum = 0
    var even = false
    for (index in length - 1 downTo 0) {
        var digit = digits[index]
        if (even) {
            digit *= multiplication
        }
        if (digit > 9) {
            digit = digit / 10 + digit % 10
        }
        sum += digit
        even = !even
    }
    return sum % 10 == 0
}

fun Activity.launchWebIntent(url: String) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse(url)
    intent.resolveActivity(packageManager)?.let {
        startActivity(intent)
    }
}

fun ImageView.setDrawable(@DrawableRes drawable: Int) {
    this.setImageDrawable(ContextCompat.getDrawable(this.context, drawable))
}

inline fun <T> Iterable<T>.firstElement(predicate: (T) -> Boolean): T? {
    for (element in this) {
        if (predicate(element)) {
            return element
        }
    }
    return null
}

fun <T> List<T>.firstElement(): T? =
    if (isEmpty()) {
        null
    } else {
        this[0]
    }

fun <T> List<T>.lastElement(): T? =
    if (isEmpty()) {
        null
    } else {
        this[this.size - 1]
    }

fun <T> List<T>.lastIndexPosition(): Int =
    if (isEmpty()) -1 else this.size - 1

fun <T> MutableList<T>.removeLastElement() {
    if (this.isEmpty()) {
        return
    } else {
        removeAt(lastIndexPosition())
    }
}

fun EditText.searchEvent(timeout: Long): LiveData<String> {
    val searchObserver = MutableLiveData<String>()
    RxTextView.afterTextChangeEvents(this)
        .debounce(timeout, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            searchObserver.value = it.editable().toString()
        }
    return searchObserver
}


fun String.getFirstWord(): String = when {
    this.contains(" ", true) -> this.substringBetween(0, " ")
    this.contains("\n", true) -> this.substringBetween(0, "\n")
    else -> this
}

fun String.getDateContentWithoutTimeZone(): Long =
    this.substringBetween("(", ")").toLong()

@SuppressLint("CheckResult")
fun Activity.disableUserInteraction() {
    window.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
    )
    Completable.timer(1000, TimeUnit.MILLISECONDS)
        .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
        .subscribe({
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }, {
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        })
}

fun String.firstCharacter(): String {
    return if (this.trim().isEmpty()) {
        ""
    } else {
        this[0].toString()
    }
}

fun Date.getCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar
}

fun Calendar.getTimeInHours(): String {
    var hour = this.get(Calendar.HOUR).toString()
    if (hour.length == 1) {
        hour = "0$hour"
    }

    var minutes = this.get(Calendar.MINUTE).toString()
    if (minutes.length == 1) {
        minutes = "0$minutes"
    }

    return "$hour:$minutes"

}

fun String.formatFlightNumberProperly(): String = if (this.length == 4) {
    this
} else {
    " ${this}"
}

fun String.formatWithDoubleZero(): String {
    return if (this.trim().isNotEmpty() && this.contains(".")) {
        val secondPart = this.substringAfter(".")
        if (secondPart.length == 2) {
            this
        } else {
            val firstPart = this.substringBefore(".")
            firstPart.plus(".").plus(secondPart).plus("0")
        }
    } else {
        this
    }
}

fun Date.formatTimeZone(): String {
    val timeZone = TimeZone.getDefault()
    val offset = timeZone.getOffset(this.time) / 3600000.0
    val offsetString = offset.toString()

    val hoursStr = offsetString.split(".")[0]
    val minStr = offsetString.split(".")[1]

    var hours = hoursStr.toInt()
    val decimalMinutes = minStr.toDouble() * .1

    val isPositive = hours >= 0

    hours = if (isPositive) hours else hours * -1

    val minutes = (decimalMinutes * 60).toInt()

    val timeZoneString =
        (if (hours > 9) hours.toString() else "0$hours") + (if (minutes > 9) minutes.toString() else "0$minutes")
    return (if (isPositive) "+" else "-") + timeZoneString
}

fun Date.formatDateToService(): String = "/Date(${this.time}${this.formatTimeZone()})/"


private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()

fun CharSequence.unaccent(): String {
    val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
    return REGEX_UNACCENT.replace(temp, "")
}

fun String.capitalizeWords(): String {

    val words = this.split(" ").toMutableList()

    var output = ""

    for (word in words) {
        output += word.toLowerCase().capitalize() + " "
    }

    return output.trim()
}

fun androidx.recyclerview.widget.SnapHelper.getSnapPosition(recyclerView: androidx.recyclerview.widget.RecyclerView): Int {
    val layoutManager =
        recyclerView.layoutManager ?: return androidx.recyclerview.widget.RecyclerView.NO_POSITION
    val snapView =
        findSnapView(layoutManager) ?: return androidx.recyclerview.widget.RecyclerView.NO_POSITION
    return layoutManager.getPosition(snapView)
}

fun View.calculateRectOnScreen(): RectF {
    val location: IntArray = IntArray(2)
    this.getLocationOnScreen(location)
    return RectF(
        location[0].toFloat(),
        location[1].toFloat(),
        (location[0] + this.measuredWidth).toFloat(),
        (location[1] + this.measuredHeight).toFloat()
    )
}

fun View.slideUp(distance: Float) {
    val animation = ObjectAnimator.ofFloat(this, "translationY", -distance)
    animation.duration = 400
    animation.start()
}

fun View.slideDown(distance: Float) {
    val animation = ObjectAnimator.ofFloat(this, "translationY", distance)
    animation.duration = 400
    animation.start()
}

fun FragmentManager.removeFragment(fragment: Fragment) {
    this.beginTransaction().remove(fragment).commit()
    this.popBackStack()
}

fun String.highlightText(@ColorRes color: Int, context: Context): Spanned {
    val hexColor = context.extractHexColorFromResource(color)
    var resultText = ""
    var isHighlighted = false

    for (char in this) {
        with(char.toString()) {
            when (this) {
                "(" -> {
                    isHighlighted = true
                }
                ")" -> {
                    isHighlighted = false
                }
                else -> {
                    resultText += if (isHighlighted) {
                        val parsed = "<font color='$hexColor'>$this</font>"
                        parsed
                    } else {
                        this
                    }
                }
            }
        }
    }

    return resultText.parseAsHtml()
}

fun Context?.extractHexColorFromResource(color: Int): String = if (this != null) {
    "#" + Integer.toHexString(ContextCompat.getColor(this, color) and 0x00ffffff)
} else {
    "#6cace4"
}

//Only when the date from server has no timezone
fun String.getDateFromServerMillis(): Date {
    val calendar: Calendar = Calendar.getInstance()
    val millis = this.replace("/Date(", "").replace(")/", "")
    calendar.timeInMillis = millis.toLong()
    return calendar.time
}

inline fun validateInput(
    inputLayout: TextInputLayout,
    inputView: TextView,
    skip: Int = 1,
    crossinline body: () -> Unit
): Subscription {
    return RxView.focusChanges(inputView)
        .skip(skip) // Listen for focus events.
        .map {
            if (!it) { // If view lost focus, lambda (our check logic) should be applied.
                body()
            }
            return@map it
        }
        .flatMap { hasFocus ->
            RxTextView.textChanges(inputView)
                //.skip(1)
                .map {
                    if (hasFocus && inputLayout.isErrorEnabled) inputLayout.isErrorEnabled = false
                    it.toString()
                } // Disable error when user typing.
                .skipWhile { hasFocus } // Don't react on text change events when we have a focus.
                .doOnEach {
                    body()
                }
        }.subscribe {}
}

inline fun validateInputWithFocusableListener(
    inputLayout: TextInputLayout,
    inputView: TextView,
    skip: Int = 1,
    crossinline focusableListener: (hasFocus: Boolean) -> Unit,
    crossinline body: () -> Unit
): Subscription {
    return RxView.focusChanges(inputView)
        .skip(skip) // Listen for focus events.
        .map {
            if (!it) { // If view lost focus, lambda (our check logic) should be applied.
                body()
            }
            return@map it
        }
        .flatMap { hasFocus ->
            RxTextView.textChanges(inputView)
                //.skip(1)
                .map {
                    if (hasFocus && inputLayout.isErrorEnabled) inputLayout.isErrorEnabled = false
                    focusableListener(hasFocus)
                    it.toString()
                } // Disable error when user typing.
                .skipWhile { hasFocus } // Don't react on text change events when we have a focus.
                .doOnEach {
                    body()
                }
        }.subscribe {}
}

fun EditText.trimmedValue(): String =
    this.text.toString().trim()

fun View.show(show: Boolean) {
    if (show) {
        this.visible()
    } else {
        this.gone()
    }

}

fun clearTextInputLayoutError(vararg inputLayouts: TextInputLayout) =
    inputLayouts.forEach { it.isErrorEnabled = false }

fun Fragment.hideKeyboard() {
    val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.windowToken, 0)
}

fun <T> LiveData<T>.observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
    observe(lifecycleOwner, object : androidx.lifecycle.Observer<T> {
        override fun onChanged(t: T?) {
            observer.onComplete()
            removeObserver(this)
        }
    })
}