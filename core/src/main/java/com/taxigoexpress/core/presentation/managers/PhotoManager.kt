package com.taxigoexpress.core.presentation.managers

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.ia.mchaveza.kotlin_library.PermissionCallback
import com.ia.mchaveza.kotlin_library.PermissionManager
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class PhotoManager(private val fragment: Fragment, private val callback: PhotoManagerCallback) : PermissionCallback {
    private val requestCameraCode = 999
    private val requestImageCode = 888
    private val activity by lazy {
        fragment.requireActivity()
    }
    private val context by lazy {
        fragment.requireContext()
    }

    private var askedPermission = Permission.Camera
    private val permissionManager by lazy { PermissionManager(activity, this) }

    fun openCamera() {
        if (permissionManager.permissionGranted(Manifest.permission.CAMERA)) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val photoFile: File? = try {
                createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                //Timber.e(ex)
                null
            }

            photoFile?.let {
                val photoURI: Uri = FileProvider.getUriForFile(
                    context,
                    "taxigoexpress.com.driver.fileprovider",
                    it
                )
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                fragment.startActivityForResult(intent, requestCameraCode)
            }

        } else {
            permissionManager.requestSinglePermission(Manifest.permission.CAMERA)
        }
    }

    fun openGallery() {

        askedPermission = Permission.Storage

        if (permissionManager.permissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "image/*"
            fragment.startActivityForResult(
                Intent.createChooser(intent, "Selecciona una foto"),
                requestImageCode
            )
        } else {
            permissionManager.requestSinglePermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

    }

    override fun onPermissionDenied(permission: String) {

    }

    override fun onPermissionGranted(permission: String) = when (askedPermission) {
        Permission.Camera -> openCamera()
        Permission.Storage -> openGallery()
    }

    private enum class Permission {
        Camera,
        Storage
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == requestImageCode) {
            data?.data?.let {
                val bitmap: Bitmap = MediaStore.Images.Media.getBitmap(activity.contentResolver, it)
                val file = File(it.path)
                callback.onGetPhotoManagerResult(bitmap, file)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == requestCameraCode) {
            getBitmapFromFile()?.let { bitmap ->
                val file = File(currentPhotoPath)
                callback.onGetPhotoManagerResult(bitmap, file)
            }
        }
    }

    private fun getBitmapFromFile(): Bitmap? {
        // Get the dimensions of the View
        val targetW = 400
        val targetH = 800

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = Math.min(photoW / targetW, photoH / targetH)

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inPurgeable = true
        }
        return BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
    }

    private lateinit var currentPhotoPath: String

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }


    interface PhotoManagerCallback {
        fun onGetPhotoManagerResult(bitmap: Bitmap, file: File)
    }

}