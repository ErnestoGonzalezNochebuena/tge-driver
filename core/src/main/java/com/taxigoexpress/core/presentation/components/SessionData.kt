package com.taxigoexpress.core.presentation.components

import com.taxigoexpress.core.domain.entities.UserEntity

object SessionData {
    var driverEntity: UserEntity? = null
    var driverId: Int? = null
}
