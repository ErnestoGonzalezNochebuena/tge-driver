package com.taxigoexpress.core.presentation.components

import android.accounts.AccountManager
import android.app.Application
import android.content.res.Resources
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.presentation.authentication.services.KeyStoreManager
import com.taxigoexpress.core.presentation.modules.ApplicationModule
import com.taxigoexpress.core.presentation.modules.NetModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetModule::class])
interface ApplicationComponent {

    fun applicationContext(): Application

    fun sharedPreferences(): SharedPreferencesManager

    fun resources(): Resources

    fun keyStoreManager(): KeyStoreManager

    fun trackingManager(): TrackingManager

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    @Named("main_retrofit")
    fun mainRetrofit(): Retrofit

    fun accountManager(): AccountManager

}