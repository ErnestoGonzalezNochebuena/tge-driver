package com.taxigoexpress.core.presentation.utils

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior


class LockBottomSheetBehavior<itemView : View> : BottomSheetBehavior<View> {

    private var mAllowUserDragging = true

    constructor() : super() {
    }

    constructor (context: Context, attrs: AttributeSet?) : super(context, attrs) {
    }

    fun setAllowUserDragging(allowUserDragging: Boolean) {
        mAllowUserDragging = allowUserDragging
    }

    override fun onInterceptTouchEvent(
        parent: CoordinatorLayout,
        child: View,
        event: MotionEvent
    ): Boolean {
        return if (!mAllowUserDragging) {
            false
        } else super.onInterceptTouchEvent(parent, child, event)
    }

}