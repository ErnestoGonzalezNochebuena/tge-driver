package com.taxigoexpress.core.presentation.utils

object Constants {
    const val CURRENT_TRIP = "current.trip"
    const val CURRENT_PASSENGER = "current.passenger"
    const val ONLINE = "online"
    const val DRIVER_ID = "driver.id"
    const val CAR_INFO = "car.info"
    const val DRIVER_INFO = "driver.info"
    const val ENDED_REGISTER = "ended.register"
    const val PASSWORD = "p455"
    const val PASS = "tge.pass.id"
    const val USER = "tge.user"
    const val IS_LOGGED = "is.logged"
    const val LOADING_MESSAGE = "loading.message"
    const val CARD_SCAN_REQUEST_CODE = 1076

}