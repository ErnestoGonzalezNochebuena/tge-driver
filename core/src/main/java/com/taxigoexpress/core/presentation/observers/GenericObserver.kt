package com.taxigoexpress.core.presentation.observers

import android.view.View
import com.taxigoexpress.core.R
import com.taxigoexpress.core.presentation.view.BaseLiveData
import java.io.IOException

class GenericObserver<T>(private val liveData: BaseLiveData<T>) :
    DefaultObserverLiveData<T>(liveData) {

    override fun onStart() {
        liveData.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        liveData.loadingObserver.value = View.GONE
    }

    override fun onNext(t: T) {
        liveData.customObserver.value = t
    }

    override fun onError(e: Throwable) {
        onComplete()
        when (e) {
            is IOException -> liveData.showErrorObserver.value = R.string.no_internet_connection
            else -> liveData.showExceptionObserver.value = e.localizedMessage
        }
    }

}