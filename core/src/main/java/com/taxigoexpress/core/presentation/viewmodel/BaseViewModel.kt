package com.taxigoexpress.core.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taxigoexpress.core.presentation.view.BaseLiveData

abstract class BaseViewModel : ViewModel() {

    fun <Q> buildLiveData(): BaseLiveData<Q> =
        BaseLiveData(MutableLiveData<Q>(), MutableLiveData(), MutableLiveData(), MutableLiveData())

}