package com.taxigoexpress.core.presentation.managers

import com.taxigoexpress.core.domain.exceptions.TGException
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.Response

object ResponseManager {

    /**
     * Obtains a failure response out of a
     * 200 OK when it exists
     * @param response
     */
    fun processWrongBody(response: Response<*>): Throwable {
        return try {
            val data = response.errorBody()?.string()
            val jObjError = JSONObject(data)
            TGException(jObjError.getString("message"),jObjError.getString("status"))
        } catch (ioException: Exception) {
            TGException("Error parsing response", ioException)
        }
    }

    fun <T> Response<T>.handleResponse() = if (this.isSuccessful) {
        Observable.just(this.body())
    } else {
        Observable.error(ResponseManager.processWrongBody(this))
    }

}