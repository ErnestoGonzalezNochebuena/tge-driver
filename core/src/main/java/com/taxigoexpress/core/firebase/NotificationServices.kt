package com.taxigoexpress.core.firebase

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.taxigoexpress.core.domain.entities.NotificationEntity
import com.taxigoexpress.core.managers.TaxiNotificationManager
import org.json.JSONException

class NotificationServices : FirebaseMessagingService() {

    companion object {
        /**
         * come back the FCMToken
         *
         * @param senderId is an identifier to locate the FCMToken
         * @param finishCallback interface that tells us when it ends
         */
        fun getFCMToken(senderId: String, finishCallback: (result: String) -> Unit) {

            val task = @SuppressLint("StaticFieldLeak")
            object : AsyncTask<Void, String, String>() {

                override fun doInBackground(vararg params: Void?): String {
                    return try {
                        FirebaseInstanceId.getInstance()
                            .getToken(senderId, FirebaseMessaging.INSTANCE_ID_SCOPE).toString()
                    } catch (e: Exception) {
                        ""
                    }
                }

                override fun onPostExecute(result: String) {
                    finishCallback(result)
                }
            }
            task.execute()
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val entity = NotificationEntity()

        message.notification?.let { n ->

            entity.body = n.body.toString()
            entity.title = n.title.toString()

            TaxiNotificationManager.getInstance().createNoticeNotification(this.baseContext,
                entity
            )
        }

        val data = message.data

        if (data.isNotEmpty()) {
            try {
                entity.body = data["body"].toString()
                entity.title = data["title"].toString()
            } catch (e: JSONException) {
                Log.e("JSON_CONVERT", e.message)
            }

            TaxiNotificationManager.getInstance().createNoticeNotification(this.baseContext,
                entity
            )
        }
    }
}