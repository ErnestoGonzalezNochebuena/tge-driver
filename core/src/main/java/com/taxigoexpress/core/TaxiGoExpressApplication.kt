package com.taxigoexpress.core

import android.content.Context
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.taxigoexpress.core.firebase.NotificationServices
import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.components.DaggerApplicationComponent
import com.taxigoexpress.core.presentation.modules.ApplicationModule
import com.taxigoexpress.core.presentation.modules.NetModule

class TaxiGoExpressApplication : MultiDexApplication() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .netModule(NetModule(this.getString(R.string.base_url)))
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("GHP", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token
                token?.let { senderID ->
                    NotificationServices.getFCMToken(senderID) { FCMToken ->
                        if (FCMToken.isNotEmpty()) {
                            return@getFCMToken
                        }
                    }
                }
                // Log and toast
                val msg = getString(R.string.msg_token_fmt, token)
                Log.d("GHP", msg)
            })
    }

}

/**
 * @return The application component from any class that have access or extends the Context class
 */
fun Context.getApplicationComponent(): ApplicationComponent =
    (this.applicationContext as TaxiGoExpressApplication).applicationComponent