package com.taxigoexpress.core.managers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.gson.Gson
import com.taxigoexpress.core.R
import com.taxigoexpress.core.domain.entities.NotificationEntity
import com.taxigoexpress.core.domain.entities.TripEntity
import java.util.*


class TaxiNotificationManager {


    companion object {
        private const val NOTIFICATION_ID = 6578
        private val random = Random()
        private const val NOTIFICATION_CHANNEL_ID = "NOTIFICATION_CHANNEL_ID"
        private const val CHANNEL_NAME_CODI = "Taxi go express"
        private const val REQUEST_CODE = 1

        private var sharedInstance: TaxiNotificationManager? = null

        /**
         * Create an instance of our object
         */
        fun getInstance(): TaxiNotificationManager {
            if (sharedInstance == null) {
                sharedInstance = TaxiNotificationManager()
            }

            return sharedInstance!!
        }
    }

    fun createNoticeNotification(context: Context, entity: NotificationEntity) {

        val notificationManager: NotificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val descriptionChannel = context.getString(R.string.notification_description_channel)
            val importance = NotificationManager.IMPORTANCE_DEFAULT

            val channel =
                NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    CHANNEL_NAME_CODI, importance
                ).apply {
                    description = descriptionChannel
                }

            channel.enableLights(true)
            channel.enableVibration(true)

            notificationManager.createNotificationChannel(channel)
        }

        val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)

        val trip = Gson().fromJson(entity.body, TripEntity::class.java)
        val intent = Intent(Intent.ACTION_VIEW).apply {
            setClassName(context, "taxigoexpress.com.driver.trips.presentation.activities.TripActivity")
        } // Here pass your activity where you want to redirect.


        intent.putExtra("trip", trip)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val contentIntent =
            PendingIntent.getActivity(context, (Math.random() * 100).toInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT)

        notificationBuilder.setContentText("Viaje de ${trip._domicilioOrigen} a ${trip._domicilioDestino}")
            .setContentTitle("Nuevo viaje solicitado")
            .setSmallIcon(R.drawable.ic_car_activate_service)
            .setAutoCancel(true)
            .setContentIntent(contentIntent)
            .priority = NotificationCompat.PRIORITY_HIGH

        val style = NotificationCompat.BigTextStyle(notificationBuilder)
        style.bigText("Viaje de ${trip._domicilioOrigen} a ${trip._domicilioDestino}")
            .setBigContentTitle("Nuevo viaje solicitado")

        val notification = notificationBuilder.build()

        val notifManager = NotificationManagerCompat.from(context)
        val id = random.nextInt() + NOTIFICATION_ID


        notifManager.notify(id, notification)
    }
}