package com.taxigoexpress.register.data.datasourceImplementations

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.register.data.services.RegisterServices
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.components.SessionData
import com.taxigoexpress.register.domain.datasourceAbstractions.RegisterDataSource
import com.taxigoexpress.register.domain.entities.KeysEntity
import io.reactivex.Observable
import javax.inject.Inject

class RegisterDataSourceImpl
@Inject constructor(private val service: RegisterServices) : RegisterDataSource {

    override fun registerUser(request: UserEntity): Observable<UserEntity> =
        this.service.register(request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun registerDriver(userId: String, request: UserEntity): Observable<UserEntity> =
        this.service.registerDriver(userId, request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun registerCar(params: CarEntity): Observable<CarEntity> =
        this.service.registerCar(params.idCond, params).flatMap {
            if (it.isSuccessful) {
                Observable.just(params)
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun getAwsKeys(): Observable<KeysEntity> =
        this.service.getAwsKeys().flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body()?.first())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }
}
