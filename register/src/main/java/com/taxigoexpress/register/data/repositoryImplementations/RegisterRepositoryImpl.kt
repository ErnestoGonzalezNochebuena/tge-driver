package com.taxigoexpress.register.data.repositoryImplementations

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.register.data.datasourceImplementations.RegisterDataSourceImpl
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.entities.KeysEntity
import com.taxigoexpress.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import javax.inject.Inject

class RegisterRepositoryImpl
@Inject constructor(private val registerDataSourceImpl: RegisterDataSourceImpl) :
    RegisterRepository {

    override fun registerUser(userEntity: UserEntity): Observable<UserEntity> =
        this.registerDataSourceImpl.registerUser(userEntity)

    override fun registerDriver(userEntity: UserEntity): Observable<UserEntity> =
        this.registerDataSourceImpl.registerDriver(
            userEntity.id.toString(),
            userEntity
        )

    override fun registerCar(params: CarEntity): Observable<CarEntity> =
        this.registerDataSourceImpl.registerCar(params)

    override fun getAwsKeys(): Observable<KeysEntity>  =
        this.registerDataSourceImpl.getAwsKeys()


}