package com.taxigoexpress.register.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.entities.KeysEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface RegisterServices {

    @POST(BuildConfig.REGISTER_USER)
    fun register(
        @Body request: UserEntity
    ): Observable<Response<UserEntity>>

    @POST(BuildConfig.REGISTER_DRIVER)
    fun registerDriver(
        @Path("id") userId: String,
        @Body request: UserEntity
    ): Observable<Response<UserEntity>>

    @POST(BuildConfig.REGISTER_CAR)
    fun registerCar(
        @Path("id_cond") userId: String,
        @Body request: CarEntity
    ): Observable<Response<Unit>>

    @POST(BuildConfig.GET_AWS_KEYS)
    fun getAwsKeys(
    ): Observable<Response<List<KeysEntity>>>

}