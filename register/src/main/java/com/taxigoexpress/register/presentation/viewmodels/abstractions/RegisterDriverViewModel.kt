package com.taxigoexpress.register.presentation.viewmodels.abstractions

import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.presentation.liveData.RegisterLiveData

interface RegisterDriverViewModel<T>:PresenterLiveData<T> {
    fun register(request: UserEntity)
    fun observeRegister(): RegisterLiveData
}