package com.taxigoexpress.register.presentation.fragments


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.ia.mchaveza.kotlin_library.visible
import com.jakewharton.rxbinding.view.enabled
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.register.R
import com.taxigoexpress.register.presentation.aws.AwsCore
import com.taxigoexpress.register.presentation.aws.Document
import com.taxigoexpress.register.presentation.utils.DocumentsUtils
//import com.taxigoexpress.scanlibrary.ScanActivity
//import com.taxigoexpress.scanlibrary.ScanConstants
import kotlinx.android.synthetic.main.fragment_documents_layout.*
import java.io.File
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class DocumentsFragment : BaseFragment(), AwsCore.UploadCallback {

    private lateinit var file: File
    private var awsCore: AwsCore? = null
    private lateinit var documentType: Document

    override fun getLayout(): Int = R.layout.fragment_documents_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadArguments()
        this.setClickListeners()
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.documentType = it.getSerializable(DOCUMENT_TYPE) as Document
        }
    }

    private fun setClickListeners() {
        this.btnNextDocument.clickEvent().observe(this, Observer {
            this.awsCore?.uploadDocument(
                this.documentType,
                this.file,
                this.generateDocumentPath(),
                this
            )
        })

        this.btnTryAgain.clickEvent().observe(this, Observer {
            this.openCamera()
        })

        this.clAddDocument.clickEvent().observe(this, Observer {
            this.openCamera()
        })
    }

    private fun generateDocumentPath(): String {
        this.keyStoreManager?.retrieveDataWithSafeMode(Constants.PASS)?.let { user ->
            return user.split("@").first() + documentType + ".jpg"
        } ?: kotlin.run {
            return ""
        }
    }

    private fun openCamera() {
//        startActivityForResult<ScanActivity>(
//            987,
//            ScanConstants.OPEN_INTENT_PREFERENCE to ScanConstants.OPEN_CAMERA
//        )
    }

    override fun onSuccessUpload(document: Document) {

    }

    override fun onGetCurrentPercentage(percentage: Int) {
    }

    override fun onUploadFailed(document: Document, reason: String) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 987 && resultCode == Activity.RESULT_OK) {
//            val uri = data?.extras?.getParcelable<Uri>(ScanConstants.SCANNED_RESULT)
            val uri: Uri ?= null
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(this.activity?.contentResolver, uri)
                this.activity?.contentResolver?.delete(uri, null, null)
                this.showPreview(bitmap)
            } catch (exception: IOException) {
                this.showAlert(exception.localizedMessage)
            }
        }
    }

    private fun showPreview(mBitmap: Bitmap) {
        this.ivDocumentPreview.setImageBitmap(mBitmap)
        this.ivDocumentPreview.visible()
        this.ivDocumentPreview.bringToFront()
        this.btnTryAgain.visible()
        this.btnNextDocument.enabled()
        this.file = DocumentsUtils.saveImage(mBitmap, this.documentType.name)
    }


    companion object {
        private const val DOCUMENT_TYPE = "document.type"

        fun newInstance(document: Document) = DocumentsFragment().apply {
            arguments = bundleOf(
                DOCUMENT_TYPE to document
            )
        }
    }
}
