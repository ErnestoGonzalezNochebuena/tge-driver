package com.taxigoexpress.register.presentation.aws

import java.io.Serializable

enum class Document:Serializable {
    License,
    InsurancePolicy,
    AddressDocument,
    CardCirculation
}