package com.taxigoexpress.register.presentation.modules

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.register.data.repositoryImplementations.RegisterRepositoryImpl
import com.taxigoexpress.register.data.services.RegisterServices
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.repositoryAbstractions.RegisterRepository
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterCarViewModel
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterDriverViewModel
import com.taxigoexpress.register.presentation.viewmodels.implementations.RegisterCarViewModelImpl
import com.taxigoexpress.register.presentation.viewmodels.implementations.RegisterDriverViewModelImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class RegisterModule {

    @Provides
    @FragmentScope
    fun providesRegisterServices(@Named("main_retrofit") retrofit: Retrofit): RegisterServices =
        retrofit.create(RegisterServices::class.java)

    @Provides
    @FragmentScope
    fun providesRegisterRepository(RegisterRepositoryImpl: RegisterRepositoryImpl): RegisterRepository =
        RegisterRepositoryImpl

    @Provides
    @FragmentScope
    fun providesRegisterViewmodel(registerViewModelImpl: RegisterDriverViewModelImpl): RegisterDriverViewModel<UserEntity> =
        registerViewModelImpl

    @Provides
    @FragmentScope
    fun providesRegisterCarViewmodel(registerCarViewModelImpl: RegisterCarViewModelImpl): RegisterCarViewModel<CarEntity> =
        registerCarViewModelImpl
}