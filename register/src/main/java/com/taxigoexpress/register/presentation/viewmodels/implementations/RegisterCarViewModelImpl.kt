package com.taxigoexpress.register.presentation.viewmodels.implementations

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import com.taxigoexpress.register.domain.useCases.RegisterCarUseCase
import com.taxigoexpress.register.presentation.liveData.RegisterCarLiveData
import com.taxigoexpress.register.presentation.observers.RegisterCarObserver
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterCarViewModel
import javax.inject.Inject

class RegisterCarViewModelImpl
@Inject constructor(
    private val registerCarUseCase: RegisterCarUseCase
) : BaseViewModelLiveData<CarEntity>(), RegisterCarViewModel<CarEntity> {

    private val mRegisterLiveData by lazy { RegisterCarLiveData(this.getCustomLiveData()) }

    override fun registerCar(request: CarEntity) =
        this.registerCarUseCase.execute(
            RegisterCarObserver(mRegisterLiveData),
            request
        )

    override fun observeRegisterCar(): RegisterCarLiveData =
        this.mRegisterLiveData

    override fun observeResult(): BaseLiveData<CarEntity> =
        this.mRegisterLiveData.registerCarObserver


    override fun onCleared() {
        this.registerCarUseCase.dispose()
        super.onCleared()
    }
}