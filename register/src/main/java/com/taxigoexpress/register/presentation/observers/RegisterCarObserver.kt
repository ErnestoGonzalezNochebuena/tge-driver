package com.taxigoexpress.register.presentation.observers

import android.view.View
import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.register.R
import com.taxigoexpress.register.domain.exceptions.RegisterCarException
import com.taxigoexpress.register.presentation.liveData.RegisterCarLiveData
import java.io.IOException

class RegisterCarObserver(private val mRegisterLiveData: RegisterCarLiveData) :
    DefaultObserverLiveData<CarEntity>(mRegisterLiveData.registerCarObserver) {

    override fun onStart() {
        this.mRegisterLiveData.registerCarObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mRegisterLiveData.registerCarObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: CarEntity) {
        this.mRegisterLiveData.registerCarObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mRegisterLiveData.registerCarObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mRegisterLiveData.registerCarObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is RegisterCarException -> useCase(error)
            else -> this.mRegisterLiveData.registerCarObserver.showExceptionObserver.value =
                error.localizedMessage
        }
    }

    private fun useCase(error: RegisterCarException) {
        when (error.validationType) {
            RegisterCarException.Type.NO_VALID_COLOR -> this.mRegisterLiveData.colorErrorObserver.value =
                R.string.no_valid_color
            RegisterCarException.Type.NO_VALID_REGISTER -> this.mRegisterLiveData.carRegisterObserver.value =
                R.string.no_valid_register
            RegisterCarException.Type.INVALID_MODEL -> this.mRegisterLiveData.modelErrorObserver.value =
                R.string.no_valid_model
            RegisterCarException.Type.NO_VALID_PLACES -> this.mRegisterLiveData.placesErrorObserver.value =
                R.string.no_valid_phone_number
        }
    }

}
