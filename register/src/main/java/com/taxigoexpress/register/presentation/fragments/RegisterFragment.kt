package com.taxigoexpress.register.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent

import com.taxigoexpress.register.R
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.presentation.components.DaggerRegisterComponent
import com.taxigoexpress.register.presentation.modules.RegisterModule
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterCarViewModel
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterDriverViewModel
import kotlinx.android.synthetic.main.fragment_register_layout.*
import kotlinx.android.synthetic.main.register_survey_layout.*
import org.jetbrains.anko.support.v4.intentFor
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : BaseFragment() {

    @Inject
    lateinit var registerDriverViewModel: RegisterDriverViewModel<UserEntity>

    @Inject
    lateinit var registerCarViewModel: RegisterCarViewModel<CarEntity>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    private val registerComponent by lazy {
        DaggerRegisterComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .registerModule(RegisterModule())
            .build()
    }

    override fun getLayout(): Int = R.layout.fragment_register_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.registerComponent.inject(this)
        this.observeResults()
        this.setClickListeners()
    }

    private fun setClickListeners() {
        this.btnRegister.clickEvent().observe(this, Observer {
            this@RegisterFragment.registerDriverViewModel.register(
                UserEntity(
                    null,
                    this@RegisterFragment.etName.text.toString(),
                    this@RegisterFragment.etLast.text.toString(),
                    this@RegisterFragment.etSurveyPhone.text.toString(),
                    this@RegisterFragment.etSurveyEmail.text.toString(),
                    this@RegisterFragment.etSurveyPassword.text.toString(),
                    0.0,
                    "",
                    this@RegisterFragment.etSurveyPermission.text.toString(),
                    this@RegisterFragment.etSurveyLicenseNumber.text.toString(),
                    0.0,
                    0.0,
                    this@RegisterFragment.cbTermsConditions.isChecked
                )
            )

//            this.replaceFragment(
//                DocumentsFragment.newInstance(),
//                R.id.register_content,
//                true
//            )
        })

        this.llTermsConditions.clickEvent().observe(this, Observer {
            this.cbTermsConditions.isChecked = !this.cbTermsConditions.isChecked
        })
    }

    private fun observeResults() {

        with(this.registerDriverViewModel.observeRegister()) {

            this.registerObserver.customObserver.observe(this@RegisterFragment, Observer {
                //                AccountKitManager.getCurrentAccount(this@RegisterFragment, this@RegisterFragment)
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.USER,
                    it.email
                )
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.DRIVER_ID,
                    it._id.toString()
                )
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.PASSWORD,
                    etSurveyPassword.text.toString()
                )

                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.DRIVER_INFO,
                    Gson().toJson(it, UserEntity::class.java)
                )

                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.DRIVER_ID,
                    it._id.toString()
                )

                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.CAR_INFO,
                    Gson().toJson(
                        CarEntity(
                            _numPlaces = this@RegisterFragment.etCarPassengers.text.toString()
                                .toInt(),
                            _model = this@RegisterFragment.etCarModel.text.toString(),
                            _registerCar = this@RegisterFragment.etCarRegister.text.toString(),
                            _brandCar = this@RegisterFragment.etCarBrand.text.toString(),
                            _color = this@RegisterFragment.etCarColor.text.toString(),
                            idCond = it._id.toString()
                        ), CarEntity::class.java
                    )
                )
                this@RegisterFragment.activity?.finish()
//                this@RegisterFragment.startActivity(intentFor<HomeActivity>())


//                val tracking = DocumentsTrackingEntity()
//                preferencesManager?.sharedPreferences?.edit()
//                    ?.putString(Gson().toJson(tracking), AwsKeys.DOC_TRACKING)
//                    ?.apply()


            })

            this.registerObserver.loadingObserver.observe(this@RegisterFragment, Observer {
                if (it == View.VISIBLE) {
                    this@RegisterFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@RegisterFragment.loaderDialog.dismiss()
                }
            })

            this.nameErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.lastNameErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.emailErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.phoneNumberErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.uncheckTCObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.registerObserver.showErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.registerObserver.showExceptionObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlert(it)
            })

        }

        with(this.registerCarViewModel.observeRegisterCar()) {
            this.registerCarObserver.customObserver.observe(this@RegisterFragment, Observer {

            })

            this.registerCarObserver.loadingObserver.observe(this@RegisterFragment, Observer {
                if (it == View.VISIBLE) {
                    this@RegisterFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@RegisterFragment.loaderDialog.dismiss()
                }
            })


            this.colorErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.placesErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

        }
    }

    companion object {
        fun newInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }


}
