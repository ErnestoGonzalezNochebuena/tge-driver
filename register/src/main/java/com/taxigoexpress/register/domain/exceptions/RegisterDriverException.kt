package com.taxigoexpress.register.domain.exceptions

class RegisterDriverException
    @JvmOverloads constructor(
        val validationType: Type, message: String? = "Register ${validationType.name}", cause: Throwable? = null
    ) :
            Throwable(message, cause) {

    enum class Type {
        NO_VALID_NAME,
        NO_VALID_LASTNAME,
        INVALID_PASSWORD,
        NO_VALID_EMAIL,
        NO_VALID_PHONE_NUMBER,
        UNCHECK_TC
    }
}