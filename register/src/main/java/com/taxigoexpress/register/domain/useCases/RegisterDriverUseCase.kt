package com.taxigoexpress.register.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidEmail
import com.taxigoexpress.core.presentation.utils.isValidPassword
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.exceptions.RegisterDriverException
import com.taxigoexpress.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import javax.inject.Inject

class RegisterDriverUseCase
@Inject constructor(
    private val registerRepository: RegisterRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<UserEntity, UserEntity>(threadExecutor, postExecutionThread) {


    override fun createObservable(params: UserEntity): Observable<UserEntity> = when {
        params.name.length <= 2 -> Observable.error(RegisterDriverException(RegisterDriverException.Type.NO_VALID_NAME))
        params.lastName.length <= 2 -> Observable.error(RegisterDriverException(RegisterDriverException.Type.NO_VALID_LASTNAME))
        !params.password.isValidPassword() -> Observable.error(RegisterDriverException(RegisterDriverException.Type.INVALID_PASSWORD))
        !params.email.isValidEmail() -> Observable.error(RegisterDriverException(RegisterDriverException.Type.NO_VALID_EMAIL))
        params.phoneNumber.length < 10 -> Observable.error(RegisterDriverException(RegisterDriverException.Type.NO_VALID_PHONE_NUMBER))
        params.phoneNumber.length < 10 -> Observable.error(RegisterDriverException(RegisterDriverException.Type.NO_VALID_PHONE_NUMBER))
        !params.isAcceptedTC -> Observable.error(RegisterDriverException(RegisterDriverException.Type.UNCHECK_TC))
        else -> this.registerRepository.registerUser(params).flatMap {
            this.registerRepository.registerDriver(
                this.setDataToDriver(
                    it,
                    params.password,
                    params.licenseNumber,
                    params.permission
                )
            )
        }
    }

    private fun setDataToDriver(
        userEntity: UserEntity,
        password: String,
        licenseNumber: String,
        permission: String
    ): UserEntity {
        return UserEntity(
            userEntity.id,
            userEntity.name,
            userEntity.lastName,
            userEntity.phoneNumber,
            userEntity.email,
            password,
            5.0,
            userEntity.createdDate,
            permission,
            licenseNumber
        )
    }

}