package com.taxigoexpress.register.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.taxigoexpress.core.domain.entities.BaseResponse

data class KeysEntity(
    @Expose
    @SerializedName("accessKeyId")
    val _accessKeyId: String? = null,

    @Expose
    @SerializedName("secretAccessKey")
    val _secretAccessKey: String? = null,

    @Expose
    @SerializedName("bucket")
    val _bucket: String? = null

) : BaseResponse() {

}
