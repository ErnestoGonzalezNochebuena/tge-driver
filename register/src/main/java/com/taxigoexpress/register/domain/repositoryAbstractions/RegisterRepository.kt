package com.taxigoexpress.register.domain.repositoryAbstractions

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.domain.entities.KeysEntity
import io.reactivex.Observable

interface RegisterRepository {
    fun registerUser(userEntity: UserEntity): Observable<UserEntity>
    fun registerDriver(userEntity: UserEntity): Observable<UserEntity>
    fun registerCar(params: CarEntity): Observable<CarEntity>
    fun getAwsKeys(): Observable<KeysEntity>
}
