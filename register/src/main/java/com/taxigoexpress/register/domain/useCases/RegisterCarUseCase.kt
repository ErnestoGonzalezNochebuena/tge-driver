package com.taxigoexpress.register.domain.useCases

import com.taxigoexpress.core.domain.entities.CarEntity
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.register.domain.exceptions.RegisterCarException
import com.taxigoexpress.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import javax.inject.Inject

class RegisterCarUseCase
@Inject constructor(
    private val repository: RegisterRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<CarEntity, CarEntity>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: CarEntity): Observable<CarEntity> =
        when {
            params._model.isNullOrEmpty() -> Observable.error(
                RegisterCarException(
                    RegisterCarException.Type.INVALID_MODEL
                )
            )
            params._registerCar.isNullOrEmpty() -> Observable.error(
                RegisterCarException(
                    RegisterCarException.Type.NO_VALID_REGISTER
                )
            )
            else -> this.repository.registerCar(params)
        }


}